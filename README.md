# README #

### What is this repository for? ###

###Quick summary###
 Study Planner for Software Engineering Coursework
As of 17/05/2017 the master branch contains a stable version of the code.

##IMPORTANT##
The GanntChartBranchUNSTABLE contain a branch of the code including the gantt chart.
This has been shown to occasionally throw null pointer exceptions - but due to being added hours before the deadline, debugging has not occured
### Version###
 Working toward first iteration

# Contribution guidelines #

### Writing tests###
  Tests should be always written for controller level classes
### Code Style###
 *  Names of Variables should start with a lower case, then followed by uppercase letters for each work in the name.
*   Variable names should be fairly short, but should be long enough to describe what they are, unless their meaning is unambiguous.
*   Class Names should start with upper case letters. Additionally, controllers for specific classes should be named with the suffix "Controller".
*   User interface classes should end with the suffix GUI.
*   Names of constants should be in block capitals. Constants should be used to avoid "magic numbers" in the code