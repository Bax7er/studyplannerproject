package Controller;

import Model.Coursework;
import Model.Exam;
import Model.Module;
import java.util.ArrayList;

/**
 *
 * @author James Baxter
 */
public class ModuleController {

    static ArrayList<Module> userModules;
    static Module activeModule;

    /**
     * Load modules given ArrayList of module
     *
     * @param modules ArrayList of modules
     */
    public static void loadModules(ArrayList<Module> modules) {
        userModules = modules;
        if (userModules.size() > 0) {
            activeModule = userModules.get(0);
            AssignmentController.loadAssignments(activeModule.getCoursework(), activeModule.getExams());
        }
        ArrayList<Coursework> allCW = new ArrayList<>();
        ArrayList<Exam> allExam = new ArrayList<>();
        for (Module m : userModules) {
            for (Coursework cw : m.getCoursework()) {
                allCW.add(cw);
            }
            for (Exam ex : m.getExams()) {
                allExam.add(ex);
            }
        }
        AssignmentController.loadSemesterAssignments(allCW, allExam);
    }

    /**
     * Load modules from active Semester Profile
     */
    public static void loadModules() {
        userModules = SemesterProfileController.activeProfile.getModules();
        if (userModules.size() > 0) {
            activeModule = userModules.get(0);
            AssignmentController.loadAssignments(activeModule.getCoursework(), activeModule.getExams());
        }
        ArrayList<Coursework> allCW = new ArrayList<>();
        ArrayList<Exam> allExam = new ArrayList<>();
        for (Module m : userModules) {
            for (Coursework cw : m.getCoursework()) {
                allCW.add(cw);
            }
            for (Exam ex : m.getExams()) {
                allExam.add(ex);
            }
        }
        AssignmentController.loadSemesterAssignments(allCW, allExam);
    }

    /**
     * Returns an ArrayList of string representations of the loaded modules
     *
     * @return ArrayList of string representations
     */
    public static ArrayList<String> listModuleNames() {
        ArrayList<String> moduleTitle = new ArrayList<>();
        if (userModules != null) {
            for (Module m : userModules) {
                moduleTitle.add(m.getModuleName());
            }
        }
        return moduleTitle;
    }

    /**
     * Changes the active module
     *
     * @param i index of new profile;
     */
    public static void setActiveModule(int i) {
        activeModule = userModules.get(i);
        AssignmentController.loadAssignments(activeModule.getCoursework(), activeModule.getExams());
    }

    /**
     * Add a module to the active Semester Profile
     *
     * @param m Module to add
     */
    public static void addModule(Module m) {
        userModules.add(m);
        activeModule = userModules.get(userModules.size() - 1);
        AssignmentController.loadAssignments(activeModule.getCoursework(), activeModule.getExams());
    }

    /**
     * Add a module to the active Semester Profile
     *
     * @param m Module to add
     */
    public static void addModule(String moduleCode, String title, int credit) {
        userModules.add(new Module(moduleCode, title, credit));
        activeModule = userModules.get(userModules.size() - 1);
        AssignmentController.loadAssignments(activeModule.getCoursework(), activeModule.getExams());
    }

    public static String getActiveModuleName() {
        return activeModule.getModuleName();
    }
    
    public static Module getActiveModule(){
        return activeModule;
    }
}
