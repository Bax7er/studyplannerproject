package Controller;

import Model.StudyActivity;
import java.util.ArrayList;

/**
 *
 * @author James Baxter
 */
public class StudyActivityController {
    protected static StudyActivity activeActivity;
    protected static ArrayList<StudyActivity> activityList;

    static void loadActivityList(ArrayList<StudyActivity> activities) {
       activityList = activities;
       if(activityList != null && activityList.size() >0){
           activeActivity = activityList.get(0);
       }
    }

    public static void addActivity(String text, int i, String text0) throws InvalidStudyActivityException {
        if(text.length()<1){
            throw new InvalidStudyActivityException("Name is too short");
        }
        if(i<0){
            throw new InvalidStudyActivityException("Progress cannot be negative");
        }
        activityList.add(new StudyActivity(text,StudyTaskController.activeTask.getProgType(),i,text0));
        SerializationController.save();
    }
  
    public static ArrayList<String[]> getInfoTable(){
        ArrayList<String[]> lines = new ArrayList<>();
        for(StudyActivity a : activityList){
            String prog = Integer.toString(a.getProgress());
            lines.add(new String[]{a.getDescription(),prog,a.getNotes()});
        }
        return lines;
    }

    public static void setActiveActivity(int index) {
        activeActivity = activityList.get(index);
    }

    public static String getActiveTitle() {
        return activeActivity.getDescription();
    }

    public static int getActiveProgress() {
         return activeActivity.getProgress();
    }

    public static String getActiveNotes() {
        return activeActivity.getNotes();
    }

    public static String getActiveProgressType() {
        return activeActivity.getProgType().title;
    }

    public static void setActiveTitle(String text) throws InvalidStudyActivityException {
        if(text.length()>0){
           activeActivity.setDescription(text);
            SerializationController.save();
        }
        else{
            throw new InvalidStudyActivityException("Title cannot be length 0");
        }
    }

    public static void setActiveProgress(int i) throws InvalidStudyActivityException {
       if(i>0){
           activeActivity.setProgress(i);
            SerializationController.save();
       }
       else{
           throw new InvalidStudyActivityException("Progress cannot be negative");
       }
    }

    public static void setActiveNotes(String text) {
        activeActivity.setNotes(text);
         SerializationController.save();
    }
    
    public static class InvalidStudyActivityException extends Exception {

        //Constructor that accepts a message
        public InvalidStudyActivityException(String message) {
            super(message);
        }
    }
}
