/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.SortableDeadlineObject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Baxter
 */
public class DeadlineObjectsController {
     static ArrayList<SortableDeadlineObject> deadlineObjects;
     
     /**
      * Sorts the deadlines in date order
      */
     public static void sortDeadlines(){
         Collections.sort(deadlineObjects);
         Collections.reverse(deadlineObjects);
     }
     
     /**
      * Removes all deadlines in the list that are before the given date
      * @param date date to compare
      */
     public static void getAfter(LocalDate date){
         ArrayList<SortableDeadlineObject> toRemove = new ArrayList<>();
         for(SortableDeadlineObject s : deadlineObjects){
             if(s.getDeadline().isBefore(date)){
                 toRemove.add(s);
             }
         }
         deadlineObjects.removeAll(toRemove);
     }
     
     /**
      * Removes all deadlines in the list that are after the given date
      * @param date date to compare
      */
     public static void getBefore(LocalDate date){
         ArrayList<SortableDeadlineObject> toRemove = new ArrayList<>();
         for(SortableDeadlineObject s : deadlineObjects){
             if(s.getDeadline().isAfter(date)){
                 toRemove.add(s);
             }
         }
         deadlineObjects.removeAll(toRemove);
     }
     
      /**
      * Removes deadline which do not match the given date
      * @param date date to compare
      */
     public static void getExactly(LocalDate date){
         ArrayList<SortableDeadlineObject> toRemove = new ArrayList<>();
         for(SortableDeadlineObject s : deadlineObjects){
             if(!s.getDeadline().isEqual(date)){
                toRemove.add(s);
             }
         }
         deadlineObjects.removeAll(toRemove);
     }
     
     /**
      * Removes all deadlines not matching type
      * @param type to keep
      */
     public static void saveType(SortableDeadlineObject.DeadlineType type){
         ArrayList<SortableDeadlineObject> toRemove = new ArrayList<>();
         for(SortableDeadlineObject s : deadlineObjects){
             if(!s.getType().equals(type)){
                 toRemove.add(s);
             }
         }
         deadlineObjects.removeAll(toRemove);
     }
     
     /**
      * Removes all deadlines matching type
      * @param type to remove
      */
     public static void removeType(SortableDeadlineObject.DeadlineType type){
         ArrayList<SortableDeadlineObject> toRemove = new ArrayList<>();
         for(SortableDeadlineObject s : deadlineObjects){
             if(s.getType().equals(type)){
                toRemove.add(s);
             }
         }
         deadlineObjects.removeAll(toRemove);
     }
     
     /**
      * Generates a string table for the GUI
      * @return each String [] is a line, each line is in the format
      * type,name,deadline,completion status
      */
     public static ArrayList<String[]> getTable(){
        ArrayList<String[]> lines = new ArrayList<>();
        for(SortableDeadlineObject s : deadlineObjects){
            lines.add(new String[]{s.getType().title,s.getName(),s.getDeadline().format(FileLoader.formatter),s.getCompletionStatus()});
        }
        return lines;
     }
     
     /**
      * Removes all completed deadlines
      */
     public static void removeCompleted(){
         ArrayList<SortableDeadlineObject> toRemove = new ArrayList<>();
         for(SortableDeadlineObject s : deadlineObjects){
             if(s.isCompleted()){
                 toRemove.add(s);
             }
         }
         deadlineObjects.removeAll(toRemove);
     }
     
     /**
      * Regenerates the list of deadlines
      */
     public static void reset(){
         SemesterProfileController.generateDeadlineObjects();
     }
     
}
