package Controller;

import Model.User;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

/**
 * A controller for the User study planner class
 *
 * @date 10/04/2017
 * @author James Baxter
 * @version 1
 */
public class UserController {

    public static final Color RED = new Color(193, 57, 48);
    public static final Color REDTINT = new Color(213, 75, 60);

    public static final Color ORANGE = new Color(212, 83, 0);
    public static final Color ORANGETINT = new Color(230, 127, 34);

    public static final Color GREEN = new Color(39, 174, 97);
    public static final Color GREENTINT = new Color(81, 216, 139);

    public static final Color BLUE = new Color(42, 128, 185);
    public static final Color BLUETINT = new Color(44, 151, 223);

    public static final Color PURPLE = new Color(148, 68, 173);
    public static final Color PURPLETINT = new Color(156, 86, 184);

    public static Color activeColour = BLUE;
    public static Color activeTint = BLUETINT;
    private static final int MINPASSWORDLENGTH = 5;
    private static final int MINUSERNAMELENGTH = 1;
    static User activeUser; //This is supposed to be package access
    static ArrayList<User> users = new ArrayList<>();

    /**
     * Lists all user names
     *
     * @return ArrayList of all the names of active users
     */
    public static ArrayList<String> getUserNames() {
        ArrayList<String> names = new ArrayList<>();
        for (User u : users) {
            names.add(u.getUserName());
        }
        return names;
    }

    /**
     * Adds a user to the list
     *
     * @param u User to add
     */
    public static void addUser(User u) {
        users.add(u);
    }

    /**
     * Adds a user to the list
     *
     * @param username name of user to add
     * @param password password of user to add
     */
    public static void addUser(String username, char[] password) {
        String sPassword = new String(password);
        users.add(new User(username, sPassword.hashCode()));
    }

    /**
     * Adds a user to the list
     *
     * @param username name of user to add
     * @param password password of user to add
     */
    public static void addUser(String username, String password) {
        Random rand = new Random();
        int n = rand.nextInt(4);
        users.add(new User(username, password.hashCode()));
        users.get(users.size() - 1).setColourChoice(n);
    }

    /**
     * GUI class should use this for validation of input
     *
     * @param username name of user to add
     * @param password1 password of user to add
     * @param password2 re-enter of password of user to add
     * @throws Controller.UserController.InvalidUserDetailException if user
     * couldn't be created - message contains details why
     */
    public static void addUser(String username, String password1, String password2) throws InvalidUserDetailException {
        if (!(password1.equals(password2))) {
            throw new InvalidUserDetailException("Passwords do not match");
        }
        if (!(isValidPassword(password1))) {
            throw new InvalidUserDetailException("Passwords must be " + MINPASSWORDLENGTH + " characters or longer");
        }
        if (!(UserController.isValidUserName(username))) {
            throw new InvalidUserDetailException("Username invalid. Must be unique and " + MINUSERNAMELENGTH + " characters or longer.");
        }
        users.add(new User(username, password1.hashCode()));
    }

    /**
     * GUI class should use this for validation of input
     *
     * @param username name of user to add
     * @param password1 password of user to add
     * @param password2 re-enter of password of user to add
     * @throws Controller.UserController.InvalidUserDetailException if user
     * couldn't be created - message contains details why
     */
    public static void addUser(String username, String password1, String password2, int colourChoice) throws InvalidUserDetailException {
        if (!(password1.equals(password2))) {
            throw new InvalidUserDetailException("Passwords do not match");
        }
        if (!(isValidPassword(password1))) {
            throw new InvalidUserDetailException("Passwords must be " + MINPASSWORDLENGTH + " characters or longer");
        }
        if (!(UserController.isValidUserName(username))) {
            throw new InvalidUserDetailException("Username invalid. Must be unique and " + MINUSERNAMELENGTH + " characters or longer.");
        }
        users.add(new User(username, password1.hashCode()));
        users.get(users.size() - 1).setColourChoice(colourChoice);
        SerializationController.save();
    }

    /**
     * Checks to see if the user name meets requirements for length and is
     * unique
     *
     * @param username username to check
     * @return true if valid, false otherwise
     */
    public static boolean isValidUserName(String username) {
        if (username.length() < MINUSERNAMELENGTH) {
            return false;
        }
        for (User u : users) {
            if (u.getUserName().equals(username)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks to see if a password meets requirements
     *
     * @param password password to check
     * @return true if valid, false otherwise
     */
    public static boolean isValidPassword(String password) {
        if (password.length() < MINPASSWORDLENGTH) {
            return false;
        }
        return true;
    }

    /**
     * Checks the login details for a given user index and password
     *
     * @param userNo index of user
     * @param password password for user
     * @return true is the password is correct for the given user index, false
     * otherwise
     */
    public static boolean checkPassword(int userNo, String password) {
        String sPassword = new String(password);
        int hash = users.get(userNo).getPassword();
        if (hash == sPassword.hashCode()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Logs in a user, validates details and makes the active user the selected
     * user
     *
     * @param userNo index of user
     * @param password password for user
     * @throws Controller.UserController.InvalidUserDetailException - if detail
     * do not match. A suitable message for the Gui is thrown
     */
    public static void logIn(int userNo, String password) throws InvalidUserDetailException {
        if (users.size() == 0) {
            throw new InvalidUserDetailException("No users exist, please create a new user first");
        }
        if (checkPassword(userNo, password)) {
            activeUser = users.get(userNo);
            setActiveColours(activeUser.getColourChoice());
            SemesterProfileController.LoadProfiles(activeUser.getSemesterProfiles());
        } else {
            throw new InvalidUserDetailException("Invalid Log in Details");
        }
    }

     /**
     * Deletes a user, validates details removes the selected
     * user
     *
     * @param userNo index of user
     * @param password password for user
     * @throws Controller.UserController.InvalidUserDetailException - if details
     * do not match. A suitable message for the Gui is thrown
     */
    public static void Delete(int userNo, String password) throws InvalidUserDetailException {
        if (users.size() == 0) {
            throw new InvalidUserDetailException("No users exist, please create a new user first");
        }
        if (checkPassword(userNo, password)) {
            users.remove(userNo);
            SerializationController.save();
        } else {
            throw new InvalidUserDetailException("Invalid Details");
        }
    }
    
    /**
     * Clears the list of stored users
     */
    public static void clear() {
        users.clear();
    }

    public static void setActiveUserColourChoice(int c) {
        if (activeUser != null) {
            activeUser.setColourChoice(c);
        }
    }

    public static void setActiveColours(int colourChoice) {
        switch (colourChoice) {
            case 0:
                activeColour = RED;
                activeTint = REDTINT;
                break;
            case 1:
                activeColour = ORANGE;
                activeTint = ORANGETINT;
                break;
            case 2:
                activeColour = GREEN;
                activeTint = GREENTINT;
                break;
            case 3:
                activeColour = BLUE;
                activeTint = BLUETINT;
                break;
            case 4:
                activeColour = PURPLE;
                activeTint = PURPLETINT;
                break;
            default:
                activeColour = BLUE;
                activeTint = BLUETINT;
                break;
        }
    }

    /**
     *
     * @return Minimum Password Length
     */
    public static int getMinPasswordLength() {
        return MINPASSWORDLENGTH;
    }

    /**
     *
     * @return Minimum Username Length
     */
    public static int getMinUsernameLength() {
        return MINUSERNAMELENGTH;
    }

    public static User getActiveUser() {
        return activeUser;
    }

    public static boolean hasActiveUser() {
        if (activeUser != null) {
            return true;
        }
        return false;
    }

    public static class InvalidUserDetailException extends Exception {
        //Parameterless Constructor

        public InvalidUserDetailException() {
        }

        //Constructor that accepts a message
        public InvalidUserDetailException(String message) {
            super(message);
        }
    }
}
