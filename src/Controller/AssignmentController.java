/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Assignment;
import java.util.Date;
import Model.Exam;
import Model.Coursework;
import Model.MileStone;
import Model.StudyTask;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;
import java.util.Collections;
/**
 *
 * @author Shaun Leeks
 */
public class AssignmentController {
    private static boolean activeType; 
    // true == exam.
    //false == cuarcework.
    private static Exam activeExam;
    private static Coursework activeCoursework;
    
    
    static ArrayList<Coursework> moduleCoursework = new ArrayList<>();
    static ArrayList<Exam> moduleExams= new ArrayList<>();
    
    private static ArrayList<Coursework> semesterCoursework;
    private static ArrayList<Exam> semesterExams;
    
    /**
     *
     * @param coursework
     * @param exams
     */
    public static void loadAssignments(ArrayList<Coursework> coursework, ArrayList<Exam> exams) {
        loadCoursework(coursework);
        loadExams(exams);
    }
    
    /**
     * Loads the given ArrayLists of Coursework and Exams
     * 
     * @param coursework ArrayList of Coursework to load into this semesters Coursework
     * @param exams ArrayList of Exams to load into this semesters Coursework
     */
    public static void loadSemesterAssignments(ArrayList<Coursework> coursework, ArrayList<Exam> exams) {
         semesterCoursework = coursework;
         semesterExams = exams;
    }
     
    /**
     * Loads the given ArrayList of Coursework and sets active Coursework to the first item in the list.
     * 
     * @param coursework ArrayList of Coursework to load
     */
    public static void loadCoursework(ArrayList<Coursework> coursework) {
        moduleCoursework = coursework;
        activeType = false;
        if(moduleCoursework.size()>0){
        activeCoursework = moduleCoursework.get(0);
        MileStoneController.loadMilestone(activeCoursework.getMilestones());
        }
    }
    
    /**
     * Loads the given ArrayList of Exams and sets active Exam to the first item in the list.
     * 
     * @param exams ArrayList of Exams to load
     */
    public static void loadExams(ArrayList<Exam> exams) {
        moduleExams = exams;
        activeType = true;
        if(moduleExams.size() >0){
        activeExam = moduleExams.get(0);
         MileStoneController.loadMilestone(activeExam.getMilestones());
        }
    }
    
    /**
     * Returns the Name of the current active Assignment
     * 
     * @return Name as a String
     */
    public static String getActiveAssignmentName() {
        if(activeType == true){
            // exam
            return activeExam.getAssignmentName();
        }else{
            // Coursework
            return activeCoursework.getAssignmentName();
        }
    }
    
    /**
     * Returns the Name of the current active Assignment
     * 
     * @return the type of the active Assignment as a String
     *  'Exam' if it's an Exam or 'Coursework' if it is Coursework.
     */
    public static String getActiveAssignmentType() {
        if(activeType == true){
            // exam
            return "Exam";
        }else{
            // Coursework
           return "Coursework";
        }
    }
    
    public static Assignment getActiveAssignment() {
        if(activeType == true){
            return activeExam;
        }else{
            // Coursework
           return activeCoursework;
        }
    }
    
    public static String getActiveAssignmentDateSet() {
        if(activeType == false){
            // Coursework
            return activeCoursework.getDateSet().format(FileLoader.formatter);
        }else{
            // error
           return "The ActiveAssignment is not Coursework "
                   + "so it does not have a DateDet";
        }
    }
    
    
    //Should proberly change this to int
    public static String getActiveAssignmentDuration() {
        if(activeType == true){
            // exam
            return String.valueOf(activeExam.getDuration());
        }else{
            // error
           return "The ActiveAssignment is not Coursework "
                   + "so it does not have a DateDet";
        }
    }
    
    public static String getActiveAssignmentDeadLine() {
        if(activeType == true){
            // exam
            return activeExam.getDeadline().format(FileLoader.formatter);
        }else{
            // Coursework
           return activeCoursework.getDeadline().format(FileLoader.formatter);
        }
        
    }
        
    public static ArrayList<String> listExamNames() {
        ArrayList<String> examTitle = new ArrayList<>();
        if(moduleExams != null){
            for (Exam e : moduleExams) {
                examTitle.add(e.getAssignmentName());
            }
        }
        return examTitle;
    }
    
    public static ArrayList<String> listExamNamesByDate() {
        ArrayList<String> examTitle = new ArrayList<>();
        if(moduleExams != null){
            Collections.sort(moduleExams);
            for (Exam e : moduleExams) {
                examTitle.add(e.getAssignmentName());
            }
        }
        return examTitle;
    }
    
    public static void sort(){
        Collections.sort(moduleCoursework);
        Collections.sort(moduleExams);
    }
    
    public static ArrayList<String> listCourseworkNames() {
        ArrayList<String> courseworkTitle = new ArrayList<>();
        if(moduleCoursework != null){
            for (Coursework c : moduleCoursework) {
                courseworkTitle.add(c.getAssignmentName());
            }
        }
        return courseworkTitle;
    }
    
    public static ArrayList<String> listCourseworkNamesByDate() {
        ArrayList<String> courseworkTitle = new ArrayList<>();
        if(moduleCoursework != null){
            Collections.sort(moduleCoursework);
            for (Coursework c : moduleCoursework) {
                courseworkTitle.add(c.getAssignmentName());
            }
        }
        return courseworkTitle;
    }
    /**
     *
     * @return An ArrayList of Strings containing the assignment names of the 
     * active module ordered my date then name.
     */
    public static ArrayList<String> listAssignmentNamesByDateSemester() {
        ArrayList<String> assignmentTitle = new ArrayList<>();
        //ArrayList<Coursework> tempCoursework = userCoursework;
        //ArrayList<Exam> tempExam = userExams;
        //may need to swap so at is a shadow copy or a full copy as 
        //this may reorder userCoursework and userExams.
        
        ArrayList<Assignment>  semesterAssignments = new ArrayList<>();
        semesterAssignments.addAll(semesterCoursework);
        semesterAssignments.addAll(semesterExams);
        Collections.sort(semesterAssignments);
        
        for(Assignment a : semesterAssignments){
            assignmentTitle.add(a.getAssignmentName());
        }
        return assignmentTitle;
       
    }
    
    public static ArrayList<String> listMileStonesNamesByDate(){
        ArrayList<String> mileStonesTitles = new ArrayList<>();
        
        ArrayList<Assignment>  assignments = new ArrayList<>();
        assignments.addAll(moduleCoursework);
        assignments.addAll(moduleExams);
        
        ArrayList<MileStone> mileStones = new ArrayList<>();
        for(Assignment a : assignments){
            ArrayList<MileStone> toAdd = a.getMilestones();
            if (toAdd != null){
            mileStones.addAll(a.getMilestones());
            }
        }
        
        Collections.sort(mileStones);
        
        for(MileStone m : mileStones){
            mileStonesTitles.add(m.getMilestoneName());
        }
        return mileStonesTitles;
    }
    
        public static ArrayList<String> listMileStonesNamesWithDate(){
        ArrayList<String> mileStonesTitles = new ArrayList<>();
        
        ArrayList<Assignment>  assignments = new ArrayList<>();
        assignments.addAll(moduleCoursework);
        assignments.addAll(moduleExams);
        
        ArrayList<MileStone> mileStones = new ArrayList<>();
        for(Assignment a : assignments){
            ArrayList<MileStone> toAdd = a.getMilestones();
            if (toAdd != null){
            mileStones.addAll(a.getMilestones());
            }
        }
        
        Collections.sort(mileStones);
        
        for(MileStone m : mileStones){
            mileStonesTitles.add(m.getMilestoneName() + ' '
                + m.getDeadLine().toString());
        }
        return mileStonesTitles;
    }
    
    /**
     *
     * @return An ArrayList of Strings containing the assignment names of the 
     * active module ordered my date then name.
     */
    public static ArrayList<String> listAssignmentNamesWithDateSemester() {
        ArrayList<String> assignmentTitle = new ArrayList<>();
        //ArrayList<Coursework> tempCoursework = userCoursework;
        //ArrayList<Exam> tempExam = userExams;
        //may need to swap so at is a shadow copy or a full copy as 
        //this may reorder userCoursework and userExams.
        
        ArrayList<Assignment>  semesterAssignments = new ArrayList<>();
        semesterAssignments.addAll(semesterCoursework);
        semesterAssignments.addAll(semesterExams);
        Collections.sort(semesterAssignments);
        
        for(Assignment a : semesterAssignments){
            assignmentTitle.add(a.getAssignmentName() + ' '
            + a.getDeadline().toString());
        }
        return assignmentTitle;
    }
    
        /**
     *
     * @return An ArrayList of Strings containing the assignment names of the 
     * active module ordered my date then name.
     */
    public static ArrayList<String> listAssignmentNamesByDate() {
        ArrayList<String> assignmentTitle = new ArrayList<>();
        //ArrayList<Coursework> tempCoursework = userCoursework;
        //ArrayList<Exam> tempExam = userExams;
        //may need to swap so at is a shadow copy or a full copy as 
        //this may reorder userCoursework and userExams.
        
        ArrayList<Assignment>  moduleAssignments = new ArrayList<>();
        moduleAssignments.addAll(moduleCoursework);
        moduleAssignments.addAll(moduleExams);
        Collections.sort(moduleAssignments);
        
        for(Assignment a : moduleAssignments){
            assignmentTitle.add(a.getAssignmentName());
        }
        return assignmentTitle;
      
    }
    
    public static ArrayList<String> listAssignmentNamesWithDate() {
        ArrayList<String> assignmentTitle = new ArrayList<>();
        //ArrayList<Coursework> tempCoursework = userCoursework;
        //ArrayList<Exam> tempExam = userExams;
        //may need to swap so at is a shadow copy or a full copy as 
        //this may reorder userCoursework and userExams.
        
        ArrayList<Assignment>  moduleAssignments = new ArrayList<>();
        moduleAssignments.addAll(moduleCoursework);
        moduleAssignments.addAll(moduleExams);
        Collections.sort(moduleAssignments);
        
        for(Assignment a : moduleAssignments){
            assignmentTitle.add(a.getAssignmentName() + ' '
            + a.getDeadline().toString());
        }
        return assignmentTitle;
    }
    
    
    
    public static void addCoursework(Coursework c){
        moduleCoursework.add(c);
        activeType = false;
        activeCoursework = c;
        StudyTaskController.taskList = c.getTasks();
        if(StudyTaskController.taskList.size() > 0){
            StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
        }else 
            StudyTaskController.activeTask = null;
        MileStoneController.loadMilestone(c.getMilestones());
    }
    
    public static void addCoursework(String assignmentName, LocalDate dateSet, LocalDate deadline){
        addCoursework(new Coursework(assignmentName, dateSet, deadline));
    }
    
    public static void addExam(Exam e){
        moduleExams.add(e);
        activeType = true;
        activeExam = e;
        StudyTaskController.taskList = e.getTasks();
        if(StudyTaskController.taskList.size() > 0){
            StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
        }else 
            StudyTaskController.activeTask = null;
        MileStoneController.loadMilestone(e.getMilestones());
    }
    
    public static void addExam(String assignmentName, LocalDate deadline, int duration){
        addExam(new Exam(assignmentName, deadline, duration));
    }
    
    
    /**
     * Changes the active semester profile
     *
     * @param type
     * @param i index of new profile;
     * @throws Controller.AssignmentController.InvalidAssignmentException
     */
    public static void setActiveAssignment(char type, int i) 
            throws InvalidAssignmentException {
        switch (type) {
            case 'e':
                activeType = true;
                activeExam = moduleExams.get(i);
                StudyTaskController.taskList = activeExam.getTasks();
                MileStoneController.loadMilestone(activeExam.getMilestones());
                if(StudyTaskController.taskList.size() > 0){
                    StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
                }else 
                    StudyTaskController.activeTask = null;
                break;
            case 'c':
                activeType = false;
                activeCoursework = moduleCoursework.get(i);
                StudyTaskController.taskList = activeCoursework.getTasks();
                MileStoneController.loadMilestone(activeCoursework.getMilestones());
                if(StudyTaskController.taskList.size() > 0){
                    StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
                }else 
                    StudyTaskController.activeTask = null;
                break;
            default:
                throw new InvalidAssignmentException("Invalid char entered."
                        + " Only accepts chars e for exam "
                        + "and c for coursework");
        }
        
    }
    
    public static void setActiveAssignment(char type, String title) 
            throws InvalidAssignmentException {
            boolean found = false;
            int count = 0;
        switch (type) {
            case 'e':
                while (found == false && count < moduleExams.size()){
                    if(moduleExams.get(count).getAssignmentName().equals(title)){
                        found = true;
                        activeType = true;
                        activeExam = moduleExams.get(count);
                        StudyTaskController.taskList = activeExam.getTasks();
                        MileStoneController.loadMilestone(activeExam.getMilestones());
                        if(StudyTaskController.taskList.size() > 0){
                            StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
                        }else 
                            StudyTaskController.activeTask = null;
                    }
                    count++;
                }
                break;
            case 'c':
                while (found == false && count < moduleCoursework.size()){
                    if(moduleCoursework.get(count).getAssignmentName().equals(title)){
                        found = true;
                        activeType = false;
                        activeCoursework = moduleCoursework.get(count);
                        StudyTaskController.taskList = activeCoursework.getTasks();
                        MileStoneController.loadMilestone(activeCoursework.getMilestones());
                        if(StudyTaskController.taskList.size() > 0){
                            StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
                        }else 
                            StudyTaskController.activeTask = null;
                    }
                    count++;
                }
                break;
            default:
                throw new InvalidAssignmentException("Invalid char entered."
                        + " Only accepts chars e for exam "
                        + "and c for coursework");
        }
        if(found == false){
            throw new InvalidAssignmentException("An assignment with the "
                    + "given title was not found.");
        }
    }
    
    public static void setActiveAssignment(String title) 
        throws InvalidAssignmentException {
        boolean found = false;
        int count = 0;

        while (found == false && count < moduleExams.size()){
            if(moduleExams.get(count).getAssignmentName().equals(title)){
                found = true;
                activeType = true;
                activeExam = moduleExams.get(count);
                StudyTaskController.taskList = activeExam.getTasks();
                MileStoneController.loadMilestone(activeExam.getMilestones());
                if(StudyTaskController.taskList.size() > 0){
                    StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
                }else 
                    StudyTaskController.activeTask = null;
            }
            count++;
        }
        
        count = 0;
        while (found == false && count < moduleCoursework.size()){
            if(moduleCoursework.get(count).getAssignmentName().equals(title)){
                found = true;
                activeType = false;
                activeCoursework = moduleCoursework.get(count);
                StudyTaskController.taskList = activeCoursework.getTasks();
                MileStoneController.loadMilestone(activeCoursework.getMilestones());
                if(StudyTaskController.taskList.size() > 0){
                    StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
                }else 
                    StudyTaskController.activeTask = null;
            }
            count++;
        }
        
        if(found == false){
            throw new InvalidAssignmentException("An assignment with the "
                    + "given title was not found.");
        }
    }
    
    public static ArrayList<String> getStudyTasks(){
        
        ArrayList<String> studyTaskTitle = new ArrayList<>();
        if(activeType == true){
            // exam
            if(activeExam != null & activeExam.getTasks() != null){
                for (StudyTask c : activeExam.getTasks()) {
                    studyTaskTitle.add(c.getTaskName());
                }
                return studyTaskTitle;
            }
        }else{
            // Coursework
           if(activeCoursework != null && activeCoursework.getTasks() != null){
                for (StudyTask c : activeCoursework.getTasks()) {
                    studyTaskTitle.add(c.getTaskName());
                }
                return studyTaskTitle;
            }
        }
        return null;
    }

    public static ArrayList<String> getMileStones() {
        ArrayList<String> mileStonesTitle = new ArrayList<>();
        if(activeType == true){
            // exam
            if(activeExam != null & activeExam.getMilestones() != null){
                for (MileStone c : activeExam.getMilestones()) {
                    mileStonesTitle.add(c.getMilestoneName());
                }
                return mileStonesTitle;
            }
        }else{
            // Coursework
           if(activeCoursework != null && activeCoursework.getMilestones() != null){
                for (MileStone c : activeCoursework.getMilestones()) {
                    mileStonesTitle.add(c.getMilestoneName());
                }
                return mileStonesTitle;
            }
        }
        return null;
    }

    public static LocalDate getActiveAssignmentDeadLineDate() {
       if(activeType == true){
            // exam
            return activeExam.getDeadline();
        }else{
            // Coursework
            return activeCoursework.getDeadline();
        }
    }

    
    
    public static class InvalidAssignmentException extends Exception {
        //Parameterless Constructor

        public InvalidAssignmentException() {
        }

        //Constructor that accepts a message
        public InvalidAssignmentException(String message) {
            super(message);
        }
    }
}
