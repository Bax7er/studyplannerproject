package Controller;

import Model.Coursework;
import Model.Exam;
import Model.MileStone;
import Model.Module;
import Model.SemesterProfile;
import Model.SortableDeadlineObject;
import Model.StudyTask;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author James Baxter
 */
public class SemesterProfileController {

    static ArrayList<SemesterProfile> userProfiles;
    static SemesterProfile activeProfile; //This is supposed to be package access
    static ArrayList<SortableDeadlineObject> deadlineObjects;
    
    /**
     * Loads profiles given array of profiles;
     *
     * @param profiles
     */
    public static void LoadProfiles(ArrayList<SemesterProfile> profiles) {
        userProfiles = profiles;
        if (userProfiles.size() > 0) {
            activeProfile = userProfiles.get(0);
            ModuleController.loadModules(activeProfile.getModules());
        }

    }

    /**
     * Loads profiles of active user
     */
    public static void LoadProfiles() {
        userProfiles = UserController.activeUser.getSemesterProfiles();
        if (userProfiles.size() > 0) {
            activeProfile = userProfiles.get(0);
            ModuleController.loadModules(activeProfile.getModules());
        }
    }

    /**
     * Creates a new semester profile given starting year and semester period
     *
     * @param year starting year - Must be post 2010
     * @param semesterPeriod A or B
     * @throws
     * Controller.SemesterProfileController.InvalidSemesterProfileException
     */
    public static void newProfile(int year, char semesterPeriod) throws InvalidSemesterProfileException {
        if (year < 2010) {
            throw new InvalidSemesterProfileException("Year outside of acceptable range");
        }
        if (!(semesterPeriod == 'A' || semesterPeriod == 'B')) {
            throw new InvalidSemesterProfileException("Semester Period Invalid- must be A or B");
        }
        for(SemesterProfile s: userProfiles){
            if(s.getYear() == year && s.getSemesterPeriod() == semesterPeriod){
                throw new InvalidSemesterProfileException("A semester file for "
                        +year+ " period "+semesterPeriod+" has already been loaded, try updating");
            }
        }
        userProfiles.add(new SemesterProfile(year, semesterPeriod));
        setActiveProfile(userProfiles.size() - 1);
        
    }

    /**
     * Changes the active semester profile
     *
     * @param i index of new profile;
     */
    public static void setActiveProfile(int i) {
        activeProfile = userProfiles.get(i);
        ModuleController.loadModules(activeProfile.getModules());
    }
    
    public static SemesterProfile getActiveProfile(){
        return activeProfile;
    }

    /**
     * Returns an ArrayList of string representations of the semester profile
     * @return string representations of the semester profiles
     */
    public static ArrayList<String> listProfileNames() {
        ArrayList<String> profile = new ArrayList<>();
        for (SemesterProfile p : userProfiles) {
            profile.add(p.toString());
        }
        return profile;
    }

    public static void generateDeadlineObjects(){
        deadlineObjects = new ArrayList<>();
        ArrayList<StudyTask> tasks = new ArrayList<>();
        ArrayList<MileStone> stones= new ArrayList<>();
        ArrayList<Coursework> cw= new ArrayList<>();
        ArrayList<Exam>ex= new ArrayList<>();
        for(Module m: activeProfile.getModules()){
           cw.addAll(m.getCoursework());
           ex.addAll(m.getExams());
        }
        for(Coursework c : cw){
            tasks.addAll(c.getTasks());
            stones.addAll(c.getMilestones());
            deadlineObjects.add( new SortableDeadlineObject(c.getAssignmentName(),c.getDeadline(),SortableDeadlineObject.DeadlineType.COURSEWORK,"completion not tracked",false));
        }
        for(Exam e : ex){
            tasks.addAll(e.getTasks());
            stones.addAll(e.getMilestones());
            deadlineObjects.add( new SortableDeadlineObject(e.getAssignmentName(),e.getDeadline(),SortableDeadlineObject.DeadlineType.EXAM,"completion not tracked",false));
        }
        for(StudyTask task : tasks){
            String s;
            boolean b;
            if(task.isComplete()){
                s = "Complete";
                b = true;
            }else{
                s = "Pending";
                b = false;
            }
            deadlineObjects.add( new SortableDeadlineObject(task.getTaskName(),task.getEndDate(),SortableDeadlineObject.DeadlineType.STUDYTASK,s,b));
        }
        for(MileStone mStone : stones){
            String s;
            boolean b;
            if(mStone.isCompleted()){
                s = "Complete";
                b = true;
            }else{
                s = "Pending";
                b = false;
            }
             deadlineObjects.add( new SortableDeadlineObject(mStone.getMilestoneName(),mStone.getDeadLine(),SortableDeadlineObject.DeadlineType.MILESTONE,s,b));          
        }
         DeadlineObjectsController.deadlineObjects = deadlineObjects;
    }
    public static class InvalidSemesterProfileException extends Exception {
        //Parameterless Constructor

        public InvalidSemesterProfileException() {
        }

        //Constructor that accepts a message
        public InvalidSemesterProfileException(String message) {
            super(message);
        }
    }
    
}
