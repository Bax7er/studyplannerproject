package Controller;

import Model.MileStone;
import Model.StudyTask;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author James Baxter
 */
public class MileStoneController {

    protected static MileStone activeMilestone;
    protected static ArrayList<MileStone> milestones;

    /**
     * Loads the list of milestones into the controller
     * @param milestoneList list of milestones
     */
    public static void loadMilestone(ArrayList<MileStone> milestoneList) {
        milestones = milestoneList;
    }

    /**
     * Sets the active milestone
     * @param index index of milestone within list
     */
    public static void setActiveMileStone(int index) {
        activeMilestone = milestones.get(index);
    }

    /**
     * The name of the active milestone
     * @return name
     */
    public static String getActiveMileStoneName() {
        return activeMilestone.getMilestoneName();
    }

    /**
     * The deadline of the active milestone
     * @return deadline date as string
     */
    public static String getActiveMileStoneDeadLine() {
        return activeMilestone.getDeadLine().toString();
    }

    
   /**
    * Generates a list studyTasks in the active milestone to output to the GUI
    * @return each String[] is a line in the form: Completion status, taskName
    */
    public static ArrayList<String[]> getStudyTaskCompletionTable() {
        ArrayList<String[]> lines = new ArrayList<>();
        for (StudyTask t : activeMilestone.getTasks()) {
            if (t.isComplete()) {
                lines.add(new String[]{"Completed", t.getTaskName()});
            } else {
                lines.add(new String[]{"Pending", t.getTaskName()});
            }
        }
        return lines;
    }

    /**
     * Lists all the milestone names currently loaded
     * @return list of names
     */
    public static ArrayList<String> getMilestoneNames() {
        ArrayList<String> names = new ArrayList<>();
        for (MileStone m : milestones) {
            names.add(m.getMilestoneName());
        }
        return names;
    }
    
    /**
     Lists all the milestone names currently loaded, followed by their deadlines
     * @return list of names with dates appended
     */
    public static ArrayList<String> getMilestoneNamesWithDates() {
        ArrayList<String> names = new ArrayList<>();
        for (MileStone m : milestones) {
            names.add(m.getMilestoneName()
                    +m.getDeadLine());
        }
        return names;
    }

    /**
     * Adds a milestone to the list, thus in turn the active assignment
     * @param name name of milestone
     * @param deadline completion date
     * @param dependencies list of studyTask tracked
     * @throws Controller.MileStoneController.InvalidMilestoneException 
     */
    public static void addMilestone(String name, LocalDate deadline, ArrayList<StudyTask> dependencies) throws InvalidMilestoneException {
        for(StudyTask t: dependencies){
            if(deadline.compareTo(t.getEndDate()) <0){
                throw new InvalidMilestoneException("Deadline cannot be before the deadline for the tasks");
            }
        }
        if(deadline.compareTo(AssignmentController.getActiveAssignmentDeadLineDate()) >0){
                throw new InvalidMilestoneException("Deadline cannot be before after the assignment date");
        }
        if(name.length() <1){
                throw new InvalidMilestoneException("Name cannot be blank");
        }
        if(dependencies.size() <1){
                throw new InvalidMilestoneException("Milestone must have dependencies");
        }
        MileStone milestone = new MileStone(name, deadline, dependencies);
        AssignmentController.getActiveAssignment().addMilestone(milestone);
        SerializationController.save();
    }

    /**
     * Counts how many milestones in the active list
     * @return count of milestones
     */
    public static int getTaskCount() {
        return activeMilestone.getTasks().size();
    }

    /**
     * Calculates number of completed tasks to one decimal place for current
     * active milestone
     * @return number of completed tasks
     */
    public static float getCompletedDecimal() {
        float i = 0;
        for (StudyTask task : activeMilestone.getTasks()) {
            if (task.isComplete()) {
                i++;
            }
            else{
                i+=task.getProgressDecimal();
            }
        }
        return i;
    }
    
    /**
     * Calculates how many tasks are completed in the current milestone
     * @return number of completed tasks
     */
    public static int getCompletedCount() {
        int i = 0;
        for (StudyTask task : activeMilestone.getTasks()) {
            if (task.isComplete()) {
                i++;
            }
        }
        return i;
    }

    /**
     * Returns the deadline for the current active milestone
     * @return 
     */
    public static String getActiveMileStoneDeadline() {
        return activeMilestone.getDeadLine().format(FileLoader.formatter);
    }

    /**
     * Edits the active milestone name (Saves changes to file immediately)
     * @param text new name
     * @throws Controller.MileStoneController.InvalidMilestoneException 
     */
    public static void setActiveMilestoneName(String text) throws InvalidMilestoneException {
        if(text.length() <1){
                throw new InvalidMilestoneException("Name cannot be blank");
        }
        activeMilestone.setMilestoneName(text);
       SerializationController.save();
    }

    /**
     * Edits the active milestone deadline (Saves changes to file immediately)
     * @param deadline new deadline
     * @throws Controller.MileStoneController.InvalidMilestoneException 
     */
    public static void setActiveMilestoneDeadline(LocalDate deadline) throws InvalidMilestoneException {
        for(StudyTask t: activeMilestone.getTasks()){
            if(deadline.compareTo(t.getEndDate()) <0){
                throw new InvalidMilestoneException("Deadline cannot be before the deadline for the tasks");
            }
        }
        if(deadline.compareTo(AssignmentController.getActiveAssignmentDeadLineDate()) >0){
                throw new InvalidMilestoneException("Deadline cannot be after the assignment date");
        }
        activeMilestone.setDeadLine(deadline);
        SerializationController.save();
    }

    /**
     * Edits the active milestone task list (Saves changes to file immediately)
     * @param dependencies list of studyTasks tracked
     * @throws Controller.MileStoneController.InvalidMilestoneException 
     */
    public static void setActiveMilestoneDependencies(ArrayList<StudyTask> dependencies) throws InvalidMilestoneException {
        for(StudyTask t: dependencies){
            if(activeMilestone.getDeadLine().compareTo(t.getEndDate()) <0){
                throw new InvalidMilestoneException("Deadline cannot be before the deadline for the tasks");
            }
        }
        if(dependencies.size() <1){
                throw new InvalidMilestoneException("Milestone must have dependencies");
        }
        activeMilestone.setTasks(dependencies);
        SerializationController.save();
    }
    
    public static class InvalidMilestoneException extends Exception {

        //Constructor that accepts a message
        public InvalidMilestoneException(String message) {
            super(message);
        }
    }
}
