/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Exam;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shaun Leeks & James Baxter
 */
public class FileLoader {

    static String fileType = "semester file";
    static String fileExtention = "sf";
    static DateFormat df = new SimpleDateFormat("dd‘MM‘yyyy");
    public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    /**
     *
     * @return the file type that can be loaded.
     */
    public static String getFileType() {
        return fileType;
    }

    /**
     *
     * @return the file extention that can be loaded.
     */
    public static String getFileExtention() {
        return fileExtention;
    }

    //used for testing.
    public static void main(String args[]) throws SemesterProfileController.InvalidSemesterProfileException, InvalidFileException {
        UserController.addUser("Baxter", "password");
        try {
            UserController.logIn(0, "password");
        } catch (UserController.InvalidUserDetailException ex) {
            System.out.println(ex.getMessage());
        }

        //used for early testing.
        File testfile = new File("C:\\Users\\Shaun\\Documents\\NetBeansProjects\\gitv5\\studyplannerproject\\src\\Controller\\test." + fileExtention);

        boolean loadFile = loadFile(testfile);
        int yearTest = getYear(testfile);
    }

    /**
     * Reads the semester's year from the given file.
     *
     * @param file The file to read from.
     * @return int The year the semester starts.
     * @throws InvalidFileException
     */
    public static int getYear(File file) throws InvalidFileException {
        if (!file.getPath().endsWith(fileExtention)) {
            // return error;
            throw new InvalidFileException("File is incorrect type: '"
                    + fileExtention + "' expected");

        } else {
            try {
                FileReader fileReader = new FileReader(file);
                BufferedReader bufferedReader
                        = new BufferedReader(fileReader);

                String line;
                try {
                    line = bufferedReader.readLine();
                    if (!line.contains("<?sf version=\"1.0\" encoding=\"UTF-8\"?>")) {
                        throw new InvalidFileException("File has invalid version information (Line 1).");
                    }
                    line = bufferedReader.readLine();
                } catch (IOException ex) {
                    throw new InvalidFileException("Error reading line from file.");
                }
                int year = readInt("year", bufferedReader);
                try {
                    fileReader.close();
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw new InvalidFileException("Error closing file.");
                }
                return year;

            } catch (FileNotFoundException ex) {
                throw new InvalidFileException("The File (" + file.getPath() + ") was not found.");
            }
        }
    }

    /**
     * Looks for a String surrounded by the given tag on the next line in the
     * bufferedReader.
     *
     * @param tag String tag to read.
     * @param bufferedReader BufferedReader to read from.
     * @return String surrounded by the given tag.
     * @throws InvalidFileException
     */
    private static String readString(String tag, BufferedReader bufferedReader)
            throws InvalidFileException {
        try {
            String line = bufferedReader.readLine();
            if (!line.contains(tag)) {
                throw new InvalidFileException("Not Correct File layout. "
                        + "Could not find '" + tag + "'tag.");

            }
            int no1 = line.indexOf('<' + tag + '>');
            int no2 = line.lastIndexOf("</" + tag + '>');
            String value = line.substring(no1 + tag.length() + 2, no2);
            return value;
        } catch (IOException ex) {
            throw new InvalidFileException("Error reading line from file.");
        }
    }

    private static String readString(String tag1, String tag2,
            BufferedReader bufferedReader) throws InvalidFileException {
        try {
            String line = bufferedReader.readLine();

            if (line.contains(tag1)) {
                int no1 = line.indexOf('<' + tag1 + '>');
                int no2 = line.lastIndexOf("</" + tag1 + '>');
                String value = line.substring(no1 + tag1.length(), no2);
                return value;
            }

            if (line.contains(tag2)) {
                int no1 = line.indexOf('<' + tag2 + '>');
                int no2 = line.lastIndexOf("</" + tag2 + '>');
                String value = line.substring(no1 + tag2.length(), no2);
                return value;
            } else {
                throw new InvalidFileException("Not Correct File layout. "
                        + "Could not find '" + tag1 + " or "
                        + tag2 + "'tags.");
            }
        } catch (IOException ex) {
            throw new InvalidFileException("Error reading line from file.");
        }
    }

    private static char readChar(String tag, BufferedReader bufferedReader) throws InvalidFileException {
        try {
            String line = bufferedReader.readLine();
            if (!line.contains(tag)) {
                throw new InvalidFileException("Not Correct File layout. "
                        + "Could not find '" + tag + "'tag.");
            }
            int no1 = line.indexOf('<' + tag + '>');
            int no2 = line.lastIndexOf("</" + tag + '>');
            char value = line.charAt(no1 + tag.length() + 2);
            return value;
        } catch (IOException ex) {
            throw new InvalidFileException("Error reading line from file.");
        }
    }

    private static int readInt(String tag, BufferedReader bufferedReader) throws InvalidFileException {
        try {
            String line = bufferedReader.readLine();
            if (!line.contains(tag)) {
                throw new InvalidFileException("Not Correct File layout. "
                        + "Could not find '" + tag + "'tag.");
            }
            int no1 = line.indexOf('<' + tag + '>');
            int no2 = line.lastIndexOf("</" + tag + '>');
            String temp = line.substring(no1 + tag.length() + 2, no2);
            int value = Integer.valueOf(temp);
            return value;
        } catch (IOException ex) {
            throw new InvalidFileException("Error reading line from file.");
        }
    }

    private static LocalDate readDate(String tag, BufferedReader bufferedReader) throws InvalidFileException {
        LocalDate value;
        try {
            String line = bufferedReader.readLine();
            if (!line.contains(tag)) {
                throw new InvalidFileException("Not Correct File layout. "
                        + "Could not find '" + tag + "'tag.");
            }
            int no1 = line.indexOf('<' + tag + '>');
            int no2 = line.lastIndexOf("</" + tag + '>');
            String temp = line.substring(no1 + tag.length() + 2, no2);
            try {
                value = LocalDate.parse(temp, formatter);
                return value;
            } catch (DateTimeParseException ex) {
                Logger.getLogger(FileLoader.class.getName()).log(Level.SEVERE, null, ex);
                //return "Failed to create date.";
                throw new InvalidFileException("Failed to read date. "
                        + "It could be in an incorect format "
                        + "(expected format: " + df + ')');
            }

        } catch (IOException ex) {
            throw new InvalidFileException("Error reading line from file.");
        }
    }

    public static boolean loadFile(File file) throws SemesterProfileController.InvalidSemesterProfileException, InvalidFileException {
        //LinkedList<module> modList = new LinkedList<module>();
        String line;

        //semester
        int year;
        char semesterPeriod;

        //modules
        String code;
        String name;
        int credit;

        //assessments
        String assessName;
        LocalDate deadline;

        //exam
        int duration;
        //coursework
        LocalDate dateset;

        boolean assingType;
        if (!file.getPath().endsWith(fileExtention)) {
            throw new InvalidFileException("File is not incorrect type '"
                    + fileExtention + "' expected");
        } else {
            FileReader fileReader;
            BufferedReader bufferedReader;
            try {
                fileReader
                        = new FileReader(file);
                bufferedReader
                        = new BufferedReader(fileReader);
            } catch (FileNotFoundException ex) {
                throw new InvalidFileException("The File (" + file.getPath() + ") was not found.");
            }


            
            try {
                line = bufferedReader.readLine();
                if (!line.contains("<?sf version=\"1.0\" encoding=\"UTF-8\"?>")) {
                    throw new InvalidFileException("File has invalid version information (Line 1).");
                }


                //modules
                line = bufferedReader.readLine();
                while (line.contains("<semester>")) {

                    // Get Year
                    year = readInt("year", bufferedReader);

                    // Get Semester Period
                    semesterPeriod = readChar("semesterPeriod", bufferedReader);

                    //Add Semester
                    SemesterProfileController.newProfile(year, semesterPeriod);

                    //modules
                    line = bufferedReader.readLine();
                    
                    while (line.contains("<module>")) {
                        //end of file
                        //code{
                        code = readString("code", bufferedReader);

                        //name
                        name = readString("name", bufferedReader);

                        //credit
                        credit = readInt("credit", bufferedReader);

                        //add Module
                        ModuleController.addModule(code, name, credit);

                        //assignments
                        line = bufferedReader.readLine();
                        if (!line.contains("<assignments>")) {
                        }

                        line = bufferedReader.readLine();
                        while (line.contains("exam")
                                || line.contains("coursework")) {

                            if (line.contains("<exam>")) {
                                assingType = false;
                            } else if (line.contains("<coursework>")) {
                                assingType = true;
                            } else {
                                return false; //"Not Correct File layout.";
                            }
                            //name
                            assessName = readString("name", bufferedReader);

                            //deadline
                            deadline = readDate("deadline", bufferedReader);

                            //examOnly
                            if (assingType == false) {

                                //duration  
                                duration = readInt("duration", bufferedReader);
                                AssignmentController.addExam(assessName, deadline, duration);
                                //Exam ex = 
                                //mod.add(ex);
                            }

                            //coursework only
                            if (assingType == true) {

                                //dateset
                                dateset = readDate("dateset", bufferedReader);;
                                AssignmentController.addCoursework(assessName, dateset, deadline);
                                //coursework cu = new coursework(assessName, deadline, dateset);
                                //mod.add(cu)
                            }

                            //line = readString("exam", "coursework", file);
                            line = bufferedReader.readLine();
                            line = bufferedReader.readLine();
                        }
                        if (!line.contains("</assignments>")) {
                            return false; // "Not Correct File layout.";
                        }

                        line = bufferedReader.readLine();
                        if (!line.contains("</module>")) {
                            return false; //"Not Correct File layout.";
                        }
                        //modList.add(mod);
                        line = bufferedReader.readLine();
                    }
                    if (!line.contains("</semester>")) {
                        return false; //"Not Correct File layout.";
                    }
                    line = bufferedReader.readLine();
                }
            } catch (IOException ex) {
                throw new InvalidFileException("Error reading line from file.");
            } finally {
                try {
                    bufferedReader.close();
                } catch (IOException ex) {
                    throw new InvalidFileException("Error closing file.");
                }
                try {
                    fileReader.close();
                } catch (IOException ex) {
                    throw new InvalidFileException("Error closing file.");
                }
            }
        }
        SerializationController.save();
        return true; //"Done";
    }

    public static boolean upDatedFile(File file) throws SemesterProfileController.InvalidSemesterProfileException, InvalidFileException {
        String line;

        //semester
        int year;
        char semesterPeriod;

        //modules
        String code;
        String name;
        int credit;

        //assessments
        String assessName;
        LocalDate deadline;

        //exam
        int duration;
        //coursework
        LocalDate dateset;

        boolean assingType;
        if (!file.getPath().endsWith(fileExtention)) {
            throw new InvalidFileException("File is not incorrect type '"
                    + fileExtention + "' expected");
        } else {
            FileReader fileReader;
            BufferedReader bufferedReader;
            try {
                fileReader
                        = new FileReader(file);
                bufferedReader
                        = new BufferedReader(fileReader);
            } catch (FileNotFoundException ex) {
                throw new InvalidFileException("The File (" + file.getPath() + ") was not found.");
            }

            try {
                line = bufferedReader.readLine();
                if (!line.contains("<?sf version=\"1.0\" encoding=\"UTF-8\"?>")) {
                    throw new InvalidFileException("File has invalid version information (Line 1).");
                }

                line = bufferedReader.readLine();
                while (line.contains("<semester>")) {

                    // Get Year
                    year = readInt("year", bufferedReader);

                    // Get Semester Period
                    semesterPeriod = readChar("semesterPeriod", bufferedReader);

                    //Add Semester
                    boolean found = false;
                    int index = 0;
                    while (!found && index < SemesterProfileController.userProfiles.size()) {
                        if (year == SemesterProfileController.userProfiles.get(index).getYear()) {
                            if (semesterPeriod == SemesterProfileController.userProfiles.get(index).getSemesterPeriod()) {
                                found = true;
                                SemesterProfileController.activeProfile = SemesterProfileController.userProfiles.get(index);
                            }
                        }
                        index++;
                    }
                    if (!found) {
                        throw new InvalidFileException("Data in the file does not match an existing semester profile");
                    }
                    //modules
                    line = bufferedReader.readLine();
                    while (line.contains("<module>")) {

                        //code{
                        code = readString("code", bufferedReader);

                        //name
                        name = readString("name", bufferedReader);

                        //credit
                        credit = readInt("credit", bufferedReader);
                        found = false;
                        index = 0;
                        while (!found && index < ModuleController.userModules.size()) {
                            if (code == ModuleController.userModules.get(index).getModuleCode()) {
                                found = true;
                                SemesterProfileController.activeProfile = SemesterProfileController.userProfiles.get(index);
                            }
                            index++;
                        }
                        //assignments
                        line = bufferedReader.readLine();
                        if (!line.contains("<assignments>")) {
                        }

                        line = bufferedReader.readLine();
                        while (line.contains("exam")
                                || line.contains("coursework")) {

                            if (line.contains("<exam>")) {
                                assingType = false;
                            } else if (line.contains("<coursework>")) {
                                assingType = true;
                            } else {
                                return false; //"Not Correct File layout.";
                            }
                            //name
                            assessName = readString("name", bufferedReader);

                            //deadline
                            deadline = readDate("deadline", bufferedReader);

                            //examOnly
                            if (assingType == false) {

                                //duration  
                                duration = readInt("duration", bufferedReader);
                                found = false;
                                index = 0;
                                while (!found && index < AssignmentController.moduleExams.size()) {
                                    if (assessName.equals(AssignmentController.moduleExams.get(index).getAssignmentName())) {
                                        found = true;
                                        AssignmentController.moduleExams.get(index).setDuration(duration);
                                        AssignmentController.moduleExams.get(index).setDeadline(deadline);
                                    }
                                    index++;
                                }
                                //AssignmentController.addExam(assessName, deadline, duration);
                                //Exam ex = 
                                //mod.add(ex);
                            }

                            //coursework only
                            if (assingType == true) {

                                //dateset
                                dateset = readDate("dateset", bufferedReader);;
                                found = false;
                                index = 0;
                                while (!found && index < AssignmentController.moduleCoursework.size()) {
                                    if (assessName.equals(AssignmentController.moduleCoursework.get(index).getAssignmentName())) {
                                        found = true;
                                        AssignmentController.moduleCoursework.get(index).setDateSet(dateset);
                                        AssignmentController.moduleCoursework.get(index).setDeadline(deadline);
                                    }
                                    index++;
                                }
                                //AssignmentController.addCoursework(assessName, dateset, deadline);
                                //coursework cu = new coursework(assessName, deadline, dateset);
                                //mod.add(cu)
                            }

                            //line = readString("exam", "coursework", file);
                            line = bufferedReader.readLine();
                            line = bufferedReader.readLine();
                        }
                        if (!line.contains("</assignments>")) {
                            return false; // "Not Correct File layout.";
                        }

                        line = bufferedReader.readLine();
                        if (!line.contains("</module>")) {
                            return false; //"Not Correct File layout.";
                        }
                        //modList.add(mod);
                        line = bufferedReader.readLine();
                    }
                    if (!line.contains("</semester>")) {
                        return false; //"Not Correct File layout.";
                    }
                    line = bufferedReader.readLine();
                }
            } catch (IOException ex) {
                throw new InvalidFileException("Error reading line from file.");
            } finally {
                try {
                    bufferedReader.close();
                    fileReader.close();
                } catch (IOException ex) {
                    throw new InvalidFileException("Error closing file.");
                }
            }
        }
        SerializationController.save();
        return true; //"Done";
    }

    public static class InvalidFileException extends Exception {
        //Parameterless Constructor

        public InvalidFileException() {
        }

        //Constructor that accepts a message
        public InvalidFileException(String message) {
            super(message);
        }
    }
}
