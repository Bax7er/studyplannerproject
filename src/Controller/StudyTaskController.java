/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Assignment;
import Model.StudyTask;
import Model.StudyTask.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author
 */
public class StudyTaskController {
    
    static public ArrayList<StudyTask> taskList;
    static public StudyTask activeTask;
    public static void AddTask(Assignment assignment, String taskName,LocalDate start, LocalDate end, int target,ArrayList<StudyTask>dependencies, int tasktype, int progType){
        TaskTypes[] taskTypes =TaskTypes.values();
        TaskTypes tType = taskTypes[tasktype];
        
        ProgressTypes[] progressTypes = ProgressTypes.values();
        ProgressTypes pType = progressTypes[progType];
        
        StudyTask task = new StudyTask(taskName,start,end,target,dependencies,tType,pType);
        assignment.addTask(task);
        SerializationController.save();
    }
    
    public static void AddTask(String taskName,LocalDate start, LocalDate end, int target,ArrayList<StudyTask>dependencies, int tasktype, int progType) throws InvalidStudyTaskException{
        TaskTypes[] taskTypes =TaskTypes.values();
        TaskTypes tType = taskTypes[tasktype];
        
        ProgressTypes[] progressTypes = ProgressTypes.values();
        ProgressTypes pType = progressTypes[progType];
        
        if(end.compareTo(AssignmentController.getActiveAssignmentDeadLineDate())>0){
            throw new InvalidStudyTaskException("Deadline must be before assignment deadline");
        }
        for(StudyTask t: dependencies){
            if(end.compareTo(t.getEndDate()) <0){
                throw new InvalidStudyTaskException("Deadline cannot be before the deadline for the dependencies");
            }
        }
       StudyTask task = new StudyTask(taskName,start,end,target,dependencies,tType,pType);
       taskList.add(task);
       SerializationController.save();
    }

    public static void setActiveTask(int index) {
       activeTask = taskList.get(index);
       StudyActivityController.loadActivityList(activeTask.getActivities());
    }
    
    public static ArrayList<String> getNames(){
        ArrayList<String> names = new ArrayList<>();
        for(StudyTask s : taskList){
            names.add(s.getTaskName());
        }
        return names;
    }
    
    public static String [] getTaskTypes(){
        String [] type = new String[StudyTask.TaskTypes.values().length];
        for(TaskTypes t : StudyTask.TaskTypes.values()){
            type[t.ordinal()] = t.title;
        }
        return type;
    }

    public static String getActiveTaskTitle() {
        return activeTask.getTaskName();
    }

    public static String getActiveTaskType() {
        return activeTask.getTaskType().title;
    }

    public static String getActiveDeadline() {
        return activeTask.getEndDate().format(FileLoader.formatter);
    }

    public static ArrayList<String[]> getStudyTaskCompletionTable() {
        ArrayList<String[]> lines = new ArrayList<>();
        for (StudyTask t : activeTask.getDependencies()) {
            if (t.isComplete()) {
                lines.add(new String[]{"Completed", t.getTaskName()});
            } else {
                lines.add(new String[]{"Pending", t.getTaskName()});
            }
        }
        return lines;
    }

    public static int getActiveTaskTypeOrdinal() {
        return activeTask.getTaskType().ordinal();
    }

    public static void setActiveTaskName(String text) {
        activeTask.setTaskName(text);
        SerializationController.save();
    }

    public static void setActiveTaskDeadline(LocalDate parse) {
       activeTask.setEndDate(parse);
       SerializationController.save();
    }

    public static void setActiveTaskDependencies(ArrayList<StudyTask> dependencies) {
        activeTask.setDependencies(dependencies);
        SerializationController.save();
    }

    public static String [] getProgressTypes() {
        String [] type = new String[StudyTask.ProgressTypes.values().length];
        for(ProgressTypes t : StudyTask.ProgressTypes.values()){
            type[t.ordinal()] = t.title;
        }
        return type;
    }

    public static int getActiveTarget() {
       return activeTask.getTarget();
    }

    public static String getActiveProgressType() {
        return activeTask.getProgType().title;
    }

    public static int getActiveProgressTypeOrdinal() {
        return activeTask.getProgType().ordinal();
    }

    public static void setActiveTaskTarget(int i) {
        activeTask.setTarget(i);
        SerializationController.save();
    }
    
    public static class InvalidStudyTaskException extends Exception {

        //Constructor that accepts a message
        public InvalidStudyTaskException(String message) {
            super(message);
        }
    }
}
