/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.User;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author James Baxter
 */
public class SerializationController {

    public static void load() {
        ArrayList<User> loaded = new ArrayList<>();
        try {
            FileInputStream fileIn = new FileInputStream("data.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ArrayList tempList;
            tempList = (ArrayList) in.readObject();
            for (int i = 0; i < tempList.size(); i++) {
                loaded.add(i, (User) tempList.get(i));
            }
            in.close();
            fileIn.close();
        } catch (IOException i) {
            System.out.println("Unable to load dataFile");
           // generateDefaultProfiles();
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found,Exiting");
            //c.printStackTrace();
             System.exit(0);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
        UserController.users = loaded;
    }

    public static void save() {
        try {
            FileOutputStream fileOut = new FileOutputStream("data.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(UserController.users);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    private static void generateDefaultProfiles() {
        UserController.addUser("Baxter", "password");
        UserController.addUser("Adam", "password");
        UserController.addUser("Shaun", "password");
        UserController.addUser("Martin", "password");
        try {
            UserController.logIn(0, "password");
        } catch (UserController.InvalidUserDetailException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
