/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ViewOLD;

import Controller.AssignmentController;
import Controller.MileStoneController;
import Controller.StudyTaskController;
import java.util.ArrayList;

/**
 *
 * @author Shaun Leeks
 */
public class AssignmentGui extends javax.swing.JFrame {

    ArrayList<String> StudyTaskNames;
    
    /**
     * Creates new form AssignmentControllerGui
     */
    public AssignmentGui() {
        initComponents();
        
        AssignmentNameLabel.setText(AssignmentController.getActiveAssignmentName());
        String AssignmentType = AssignmentController.getActiveAssignmentType();
        AssignmentTypeLabel.setText("Type: " + AssignmentType);
        
        if("Exam".equals(AssignmentType)){
            AssignmentDATESETorDURATIONLable.setText("Duration: " + AssignmentController.getActiveAssignmentDuration());
        }else if ("Coursework".equals(AssignmentType)){
            AssignmentDATESETorDURATIONLable.setText("Date set: " + AssignmentController.getActiveAssignmentDateSet());
        }
        AssignmentDeadlineLabel1.setText("Deadline: " + AssignmentController.getActiveAssignmentDeadLine());
        
        updateStudyTaskList();
        
        updateMilsstoneTaskList();
    }

    public final void updateStudyTaskList(){
        ArrayList<String> task = StudyTaskController.getNames();
        if(task != null){
           String [] tasks = new String[task.size()];
            task.toArray(tasks);

            StudyTaskList.setListData(tasks); 
        }
    }
    
    public final void updateMilsstoneTaskList(){
        ArrayList<String> milestones = MileStoneController.getMilestoneNamesWithDates();
        if(milestones != null){
           String [] milsstones = new String[milestones.size()];
            milestones.toArray(milsstones);

            MileStoneList.setListData(milsstones); 
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        AssignmentNameLabel = new javax.swing.JLabel();
        backButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        AssignmentTypeLabel = new javax.swing.JLabel();
        AssignmentDeadlineLabel1 = new javax.swing.JLabel();
        AssignmentDATESETorDURATIONLable = new javax.swing.JLabel();
        AssignmentDescriptionTextField = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        StudyTaskListLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        StudyTaskList = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        MileStoneList = new javax.swing.JList<>();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        AssignmentNameLabel.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        AssignmentNameLabel.setText("ASSIGNMENT NAME");

        backButton.setText("Back");
        backButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backButtonMouseClicked(evt);
            }
        });
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        AssignmentTypeLabel.setText("ASSIGNMENT TYPE?");

        AssignmentDeadlineLabel1.setText("ASSIGNMENT DEADLINE?");

        AssignmentDATESETorDURATIONLable.setText("ASSIGNMENT DATESET/DURATION?");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(AssignmentDATESETorDURATIONLable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(AssignmentDeadlineLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(AssignmentTypeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AssignmentTypeLabel)
                .addGap(18, 18, 18)
                .addComponent(AssignmentDeadlineLabel1)
                .addGap(18, 18, 18)
                .addComponent(AssignmentDATESETorDURATIONLable)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        AssignmentDescriptionTextField.setText("Description of assignment");
        AssignmentDescriptionTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AssignmentDescriptionTextFieldActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addComponent(AssignmentDescriptionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(AssignmentDescriptionTextField)
        );

        StudyTaskListLabel.setText("Study Tasks:");

        StudyTaskList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "No study tasks were found." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        StudyTaskList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                StudyTaskListMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(StudyTaskList);

        MileStoneList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "No milestones were found." };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        MileStoneList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MileStoneListMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(MileStoneList);

        jButton1.setText("New Study Task");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("New Milestone");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(StudyTaskListLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(49, 49, 49))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(StudyTaskListLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1)
                    .addComponent(jButton2)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(AssignmentNameLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(backButton)
                        .addContainerGap())
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AssignmentNameLabel)
                    .addComponent(backButton))
                .addGap(18, 18, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        //new StudyDashboardGui().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_backButtonActionPerformed

    private void backButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backButtonMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_backButtonMouseClicked

    private void StudyTaskListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_StudyTaskListMouseClicked
        if (evt.getClickCount() == 2 && AssignmentController.getStudyTasks() != null) {
            int index = StudyTaskList.locationToIndex(evt.getPoint());
            System.out.println("index: " + index);
            StudyTaskController.setActiveTask(index);
            System.out.println("assignment: " + AssignmentController.getStudyTasks().get(index));
            new StudyTaskGui(StudyTaskGui.VIEWMODE).setVisible(true);
        }
    }//GEN-LAST:event_StudyTaskListMouseClicked

    private void AssignmentDescriptionTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AssignmentDescriptionTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_AssignmentDescriptionTextFieldActionPerformed

    private void MileStoneListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MileStoneListMouseClicked
        if (evt.getClickCount() == 2 && MileStoneController.getMilestoneNames()!= null) {
            int index = MileStoneList.locationToIndex(evt.getPoint());
            System.out.println("index: " + index);
            MileStoneController.setActiveMileStone(index);
            System.out.println("milestone: " + AssignmentController.getMileStones().get(index));
            new MileStoneGui(MileStoneGui.VIEWMODE).setVisible(true);
        }
    }//GEN-LAST:event_MileStoneListMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        new StudyTaskGui(StudyTaskGui.NEWMODE).setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
         new MileStoneGui(MileStoneGui.NEWMODE).setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AssignmentGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AssignmentGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AssignmentGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AssignmentGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AssignmentGui().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AssignmentDATESETorDURATIONLable;
    private javax.swing.JLabel AssignmentDeadlineLabel1;
    private javax.swing.JTextField AssignmentDescriptionTextField;
    private javax.swing.JLabel AssignmentNameLabel;
    private javax.swing.JLabel AssignmentTypeLabel;
    private javax.swing.JList<String> MileStoneList;
    private javax.swing.JList<String> StudyTaskList;
    private javax.swing.JLabel StudyTaskListLabel;
    private javax.swing.JButton backButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration//GEN-END:variables
}
