/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View.NewGUI;

import Controller.FileLoader;
import Controller.StudyActivityController;
import Controller.StudyTaskController;
import Controller.UserController;
import Model.StudyTask;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author Baxter
 */
public class StudyTaskGUI extends javax.swing.JFrame {

    public static enum Mode {
        NEWMODE, EDITMODE, VIEWMODE, ACTIVITYEDITMODE,ACTIVITYNEWMODE;
    };
    //protected static final int NEWMODE = 0;
  //  protected static final int EDITMODE = 1;
  //  protected static final int VIEWMODE = 2;
   // protected static final int ACTIVITYEDITMODE = 3;
  //  protected static final int ACTIVITYNEWMODE = 4;
    //private int modeSet;
    private Mode modeSet;
    AssignmentGUI parent;

    /**
     * Creates new form StudyTaskBetaGUI
     *
     * @param parent The window which created this window
     * @param mode The mode currently set(NEWMODE,EDITMODE or VIEWMODE)
     */
    public StudyTaskGUI(AssignmentGUI parent, Mode mode) {
        initComponents();
        this.parent = parent;
        this.modeSet = mode;
        //Set Table Colours
        JTableHeader header = viewStudyActivities.getTableHeader();
        header.setFont(new Font("Segoe UI Light", Font.BOLD, 14));
        header.setBackground(UserController.activeColour);
        header.setForeground(Color.WHITE);
        header = viewDependenciesTable.getTableHeader();
        header.setFont(new Font("Segoe UI Light", Font.BOLD, 14));
        header.setBackground(UserController.activeColour);
        header.setForeground(Color.WHITE);
        header = editDependenciesTable.getTableHeader();
        header.setFont(new Font("Segoe UI Light", Font.BOLD, 14));
        header.setBackground(UserController.activeColour);
        header.setForeground(Color.WHITE);
        viewStudyActivitiesScrollPane.getViewport().setBackground(UserController.activeColour);
        editDependenciesScrollPane.getViewport().setBackground(UserController.activeColour);
        viewDependenciesScrollPane.getViewport().setBackground(UserController.activeColour);
        //Hide Error text labels
        activityErrorLable.setVisible(false);
        taskErrorLable.setVisible(false);
        //Set spinner colours
        targetSpinner.getEditor().getComponent(0).setBackground(UserController.activeTint);
        targetSpinner.getEditor().getComponent(0).setForeground(Color.WHITE);

        //Load all study Tasks to dependencies list
        DefaultTableModel editDependenciesTableModel = (DefaultTableModel) editDependenciesTable.getModel();
        if (StudyTaskController.taskList != null) {
            for (StudyTask t : StudyTaskController.taskList) {
                editDependenciesTableModel.addRow(new Object[]{false, t.getTaskName()});
            }
        }
        //Load task types for dropdown
        editTaskTypes.removeAllItems();
        for (String type : StudyTaskController.getTaskTypes()) {
            editTaskTypes.addItem(type);
        }
        //Load progress types for dropdown
        editProgressTypes.removeAllItems();
        for (String type : StudyTaskController.getProgressTypes()) {
            editProgressTypes.addItem(type);
        }

        if (modeSet != Mode.NEWMODE) {
            activityProgressType.setText(StudyTaskController.getActiveProgressType());
        }
        if (modeSet == Mode.NEWMODE) {
            CardLayout card = (CardLayout) mainPanel.getLayout();
            card.show(mainPanel, "editPanel");
        }
        if (modeSet == Mode.VIEWMODE) {
            //Populate fields
            viewProgressType.setText(StudyTaskController.getActiveTarget() + StudyTaskController.getActiveProgressType());
            viewStudyTaskName.setText(StudyTaskController.getActiveTaskTitle());
            viewTaskType.setText(StudyTaskController.getActiveTaskType());
            viewDeadline.setText("Deadline: " + StudyTaskController.getActiveDeadline());
            DefaultTableModel viewDependenciesTableModel = (DefaultTableModel) viewDependenciesTable.getModel();
            for (String[] line : StudyTaskController.getStudyTaskCompletionTable()) {
                viewDependenciesTableModel.addRow(new Object[]{line[0], line[1]});
            }
            updateActivityList();
            //Set up edit window with current values
            editStudyTaskName.setText(StudyTaskController.getActiveTaskTitle());
            editDeadline.setText(StudyTaskController.getActiveDeadline());
            editTaskTypes.setSelectedIndex(StudyTaskController.getActiveTaskTypeOrdinal());
            editProgressTypes.setSelectedIndex(StudyTaskController.getActiveProgressTypeOrdinal());
            editProgressTypes.setEnabled(false);
            //Switch to view card
            switchCard(mainPanel, "viewPanel");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        taskViewPanel = new javax.swing.JPanel();
        bottomPanelView = new javax.swing.JPanel();
        newActivityButton = new javax.swing.JButton();
        viewCancelButton = new javax.swing.JButton();
        Edit = new javax.swing.JButton();
        editLabel = new javax.swing.JLabel();
        newLabel = new javax.swing.JLabel();
        closeView = new javax.swing.JLabel();
        topPanelView = new javax.swing.JPanel();
        viewStudyTaskName = new javax.swing.JLabel();
        bodyPanelView = new javax.swing.JPanel();
        viewTaskType = new javax.swing.JLabel();
        viewDeadline = new javax.swing.JLabel();
        viewDependenciesScrollPane = new javax.swing.JScrollPane();
        viewDependenciesTable = new javax.swing.JTable();
        viewStudyActivitiesScrollPane = new javax.swing.JScrollPane();
        viewStudyActivities = new javax.swing.JTable();
        StudyActivitesTableLabel = new javax.swing.JLabel();
        viewProgressType = new javax.swing.JLabel();
        taskEditPanel = new javax.swing.JPanel();
        bottomPanelEdit = new javax.swing.JPanel();
        editConfirmButton = new javax.swing.JButton();
        cancelButtonEdit = new javax.swing.JButton();
        editConfirmLabel = new javax.swing.JLabel();
        editCanelLabel = new javax.swing.JLabel();
        taskErrorLable = new javax.swing.JLabel();
        topPanelEdit = new javax.swing.JPanel();
        editStudyTaskLabel = new javax.swing.JLabel();
        bodyPanelEdit = new javax.swing.JPanel();
        editNameLabel = new javax.swing.JLabel();
        editTaskTypes = new javax.swing.JComboBox<>();
        editTaskTypeLabel = new javax.swing.JLabel();
        editDeadline = new javax.swing.JFormattedTextField();
        editDeadlineLabel = new javax.swing.JLabel();
        editDependenciesScrollPane = new javax.swing.JScrollPane();
        editDependenciesTable = new javax.swing.JTable();
        editStudyTaskName = new javax.swing.JTextField();
        editTargetLabel = new javax.swing.JLabel();
        editProgressTypes = new javax.swing.JComboBox<>();
        targetSpinner = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        ActivityPanel = new javax.swing.JPanel();
        bottomPanelEditAct = new javax.swing.JPanel();
        editConfirmButton1 = new javax.swing.JButton();
        cancelButtonEdit1 = new javax.swing.JButton();
        activityConfirmLabel = new javax.swing.JLabel();
        activityCancelLabel = new javax.swing.JLabel();
        activityErrorLable = new javax.swing.JLabel();
        topPanelEditAct = new javax.swing.JPanel();
        editActivityTitleLabel = new javax.swing.JLabel();
        bodyPanelEditAct = new javax.swing.JPanel();
        activityNameLabel = new javax.swing.JLabel();
        activityTitle = new javax.swing.JTextField();
        activityProgressType = new javax.swing.JLabel();
        progressSpinner = new javax.swing.JSpinner();
        activityNotesScrollPane = new javax.swing.JScrollPane();
        activityNotes = new javax.swing.JTextArea();
        notePaneLabel = new javax.swing.JLabel();
        activityProgress = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        mainPanel.setLayout(new java.awt.CardLayout());

        taskViewPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bottomPanelView.setBackground(UserController.activeTint);

        newActivityButton.setBackground(UserController.activeTint);
        newActivityButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.page.new.png"))); // NOI18N
        newActivityButton.setBorder(null);
        newActivityButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newActivityButtonActionPerformed(evt);
            }
        });

        viewCancelButton.setBackground(UserController.activeTint);
        viewCancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.close.png"))); // NOI18N
        viewCancelButton.setBorder(null);
        viewCancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewCancelButtonActionPerformed(evt);
            }
        });

        Edit.setBackground(UserController.activeTint);
        Edit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.page.edit.png"))); // NOI18N
        Edit.setBorder(null);
        Edit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditActionPerformed(evt);
            }
        });

        editLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        editLabel.setForeground(new java.awt.Color(255, 255, 255));
        editLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        editLabel.setText("Edit");

        newLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        newLabel.setForeground(new java.awt.Color(255, 255, 255));
        newLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        newLabel.setText("New Activity");

        closeView.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        closeView.setForeground(new java.awt.Color(255, 255, 255));
        closeView.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        closeView.setText("Close");

        javax.swing.GroupLayout bottomPanelViewLayout = new javax.swing.GroupLayout(bottomPanelView);
        bottomPanelView.setLayout(bottomPanelViewLayout);
        bottomPanelViewLayout.setHorizontalGroup(
            bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bottomPanelViewLayout.createSequentialGroup()
                .addGroup(bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(Edit, javax.swing.GroupLayout.PREFERRED_SIZE, 65, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 331, Short.MAX_VALUE)
                .addGroup(bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(newActivityButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(newLabel))
                .addGap(300, 300, 300)
                .addGroup(bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(viewCancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(closeView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        bottomPanelViewLayout.setVerticalGroup(
            bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelViewLayout.createSequentialGroup()
                .addGroup(bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(newActivityButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(Edit, javax.swing.GroupLayout.PREFERRED_SIZE, 68, Short.MAX_VALUE)
                    .addComponent(viewCancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(bottomPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editLabel)
                    .addComponent(newLabel)
                    .addComponent(closeView))
                .addContainerGap())
        );

        taskViewPanel.add(bottomPanelView, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 840, 90));

        topPanelView.setBackground(UserController.activeTint);

        viewStudyTaskName.setFont(new java.awt.Font("Segoe UI Light", 1, 36)); // NOI18N
        viewStudyTaskName.setForeground(new java.awt.Color(255, 255, 255));
        viewStudyTaskName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        viewStudyTaskName.setText("StudyTask Name");

        javax.swing.GroupLayout topPanelViewLayout = new javax.swing.GroupLayout(topPanelView);
        topPanelView.setLayout(topPanelViewLayout);
        topPanelViewLayout.setHorizontalGroup(
            topPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(viewStudyTaskName, javax.swing.GroupLayout.DEFAULT_SIZE, 840, Short.MAX_VALUE)
        );
        topPanelViewLayout.setVerticalGroup(
            topPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelViewLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(viewStudyTaskName)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        taskViewPanel.add(topPanelView, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 840, -1));

        bodyPanelView.setBackground(UserController.activeColour);

        viewTaskType.setFont(new java.awt.Font("Segoe UI Light", 1, 24)); // NOI18N
        viewTaskType.setForeground(new java.awt.Color(255, 255, 255));
        viewTaskType.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        viewTaskType.setText("Task Type");

        viewDeadline.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        viewDeadline.setForeground(new java.awt.Color(255, 255, 255));
        viewDeadline.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        viewDeadline.setText("Deadline");

        viewDependenciesTable.setAutoCreateRowSorter(true);
        viewDependenciesTable.setBackground(UserController.activeTint);
        viewDependenciesTable.setForeground(new java.awt.Color(255, 255, 255));
        viewDependenciesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Status", "Name"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        viewDependenciesTable.setEnabled(false);
        viewDependenciesTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        viewDependenciesTable.setSelectionForeground(UserController.activeTint);
        viewDependenciesScrollPane.setViewportView(viewDependenciesTable);

        viewStudyActivitiesScrollPane.setBackground(UserController.activeColour);
        viewStudyActivitiesScrollPane.setForeground(UserController.activeColour);

        viewStudyActivities.setBackground(UserController.activeTint);
        viewStudyActivities.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        viewStudyActivities.setForeground(new java.awt.Color(255, 255, 255));
        viewStudyActivities.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Study Activity Name", "Progress Value", "Notes"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        viewStudyActivities.setGridColor(UserController.activeColour);
        viewStudyActivities.setSelectionBackground(new java.awt.Color(255, 255, 255));
        viewStudyActivities.setSelectionForeground(UserController.activeTint);
        viewStudyActivities.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                viewStudyActivitiesMouseClicked(evt);
            }
        });
        viewStudyActivitiesScrollPane.setViewportView(viewStudyActivities);

        StudyActivitesTableLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        StudyActivitesTableLabel.setForeground(new java.awt.Color(255, 255, 255));
        StudyActivitesTableLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        StudyActivitesTableLabel.setText("Study Activities");

        viewProgressType.setFont(new java.awt.Font("Segoe UI Light", 1, 20)); // NOI18N
        viewProgressType.setForeground(new java.awt.Color(255, 255, 255));
        viewProgressType.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        viewProgressType.setText("ProgressType");

        javax.swing.GroupLayout bodyPanelViewLayout = new javax.swing.GroupLayout(bodyPanelView);
        bodyPanelView.setLayout(bodyPanelViewLayout);
        bodyPanelViewLayout.setHorizontalGroup(
            bodyPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelViewLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(viewProgressType, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
                    .addComponent(viewDeadline, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(viewDependenciesScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(viewTaskType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(498, Short.MAX_VALUE))
            .addGroup(bodyPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(bodyPanelViewLayout.createSequentialGroup()
                    .addContainerGap(366, Short.MAX_VALUE)
                    .addGroup(bodyPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(viewStudyActivitiesScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 433, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(StudyActivitesTableLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 21, Short.MAX_VALUE)))
        );
        bodyPanelViewLayout.setVerticalGroup(
            bodyPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelViewLayout.createSequentialGroup()
                .addComponent(viewTaskType)
                .addGap(12, 12, 12)
                .addComponent(viewProgressType)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(viewDeadline)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(viewDependenciesScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(bodyPanelViewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(bodyPanelViewLayout.createSequentialGroup()
                    .addGap(0, 5, Short.MAX_VALUE)
                    .addComponent(StudyActivitesTableLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(viewStudyActivitiesScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 11, Short.MAX_VALUE)))
        );

        taskViewPanel.add(bodyPanelView, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 840, 310));

        mainPanel.add(taskViewPanel, "viewPanel");

        taskEditPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bottomPanelEdit.setBackground(UserController.activeTint);

        editConfirmButton.setBackground(UserController.activeTint);
        editConfirmButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.check.png"))); // NOI18N
        editConfirmButton.setBorder(null);
        editConfirmButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editConfirmButtonActionPerformed(evt);
            }
        });

        cancelButtonEdit.setBackground(UserController.activeTint);
        cancelButtonEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.close.png"))); // NOI18N
        cancelButtonEdit.setBorder(null);
        cancelButtonEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonEditActionPerformed(evt);
            }
        });

        editConfirmLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        editConfirmLabel.setForeground(new java.awt.Color(255, 255, 255));
        editConfirmLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        editConfirmLabel.setText("Confirm");

        editCanelLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        editCanelLabel.setForeground(new java.awt.Color(255, 255, 255));
        editCanelLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        editCanelLabel.setText("Cancel");

        taskErrorLable.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        taskErrorLable.setForeground(new java.awt.Color(255, 255, 255));
        taskErrorLable.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        taskErrorLable.setText("**ERROR TEXT GOES HERE**");

        javax.swing.GroupLayout bottomPanelEditLayout = new javax.swing.GroupLayout(bottomPanelEdit);
        bottomPanelEdit.setLayout(bottomPanelEditLayout);
        bottomPanelEditLayout.setHorizontalGroup(
            bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editConfirmButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editConfirmLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 698, Short.MAX_VALUE)
                .addGroup(bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cancelButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editCanelLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(bottomPanelEditLayout.createSequentialGroup()
                    .addGap(106, 106, 106)
                    .addComponent(taskErrorLable, javax.swing.GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
                    .addGap(106, 106, 106)))
        );
        bottomPanelEditLayout.setVerticalGroup(
            bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelEditLayout.createSequentialGroup()
                .addGroup(bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editConfirmButton, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 3, Short.MAX_VALUE)
                .addGroup(bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editConfirmLabel)
                    .addComponent(editCanelLabel))
                .addContainerGap())
            .addGroup(bottomPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(bottomPanelEditLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(taskErrorLable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        taskEditPanel.add(bottomPanelEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 840, 90));

        topPanelEdit.setBackground(UserController.activeTint);

        editStudyTaskLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 48)); // NOI18N
        editStudyTaskLabel.setForeground(new java.awt.Color(255, 255, 255));
        editStudyTaskLabel.setText("Create/Edit Study Task");

        javax.swing.GroupLayout topPanelEditLayout = new javax.swing.GroupLayout(topPanelEdit);
        topPanelEdit.setLayout(topPanelEditLayout);
        topPanelEditLayout.setHorizontalGroup(
            topPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelEditLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editStudyTaskLabel)
                .addContainerGap(315, Short.MAX_VALUE))
        );
        topPanelEditLayout.setVerticalGroup(
            topPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelEditLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editStudyTaskLabel)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        taskEditPanel.add(topPanelEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 840, -1));

        bodyPanelEdit.setBackground(UserController.activeColour);

        editNameLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        editNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        editNameLabel.setText("StudyTask Name");

        editTaskTypes.setBackground(UserController.activeTint);
        editTaskTypes.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        editTaskTypes.setForeground(new java.awt.Color(255, 255, 255));
        editTaskTypes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        editTaskTypes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editTaskTypesActionPerformed(evt);
            }
        });

        editTaskTypeLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        editTaskTypeLabel.setForeground(new java.awt.Color(255, 255, 255));
        editTaskTypeLabel.setText("Task Type");

        editDeadline.setBackground(UserController.activeTint);
        editDeadline.setForeground(new java.awt.Color(255, 255, 255));
        editDeadline.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(new java.text.SimpleDateFormat("dd/MM/yyyy"))));
        editDeadline.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N

        editDeadlineLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        editDeadlineLabel.setForeground(new java.awt.Color(255, 255, 255));
        editDeadlineLabel.setText("Deadline");

        editDependenciesTable.setBackground(UserController.activeTint);
        editDependenciesTable.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        editDependenciesTable.setForeground(new java.awt.Color(255, 255, 255));
        editDependenciesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Depends On", "Name"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        editDependenciesTable.setSelectionBackground(new java.awt.Color(255, 255, 255));
        editDependenciesTable.setSelectionForeground(UserController.activeTint);
        editDependenciesScrollPane.setViewportView(editDependenciesTable);

        editStudyTaskName.setBackground(UserController.activeTint);
        editStudyTaskName.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        editStudyTaskName.setForeground(new java.awt.Color(255, 255, 255));
        editStudyTaskName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editStudyTaskNameActionPerformed(evt);
            }
        });

        editTargetLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        editTargetLabel.setForeground(new java.awt.Color(255, 255, 255));
        editTargetLabel.setText("Target");

        editProgressTypes.setBackground(UserController.activeTint);
        editProgressTypes.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        editProgressTypes.setForeground(new java.awt.Color(255, 255, 255));
        editProgressTypes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        editProgressTypes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editProgressTypesActionPerformed(evt);
            }
        });

        targetSpinner.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        targetSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        jLabel5.setFont(new java.awt.Font("Segoe UI Light", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Dependencies");

        javax.swing.GroupLayout bodyPanelEditLayout = new javax.swing.GroupLayout(bodyPanelEdit);
        bodyPanelEdit.setLayout(bodyPanelEditLayout);
        bodyPanelEditLayout.setHorizontalGroup(
            bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(editTaskTypeLabel)
                    .addComponent(editNameLabel)
                    .addComponent(editTargetLabel)
                    .addComponent(editDeadlineLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(bodyPanelEditLayout.createSequentialGroup()
                        .addComponent(targetSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addComponent(editProgressTypes, 0, 145, Short.MAX_VALUE))
                    .addComponent(editStudyTaskName)
                    .addComponent(editTaskTypes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editDeadline))
                .addGap(18, 40, Short.MAX_VALUE)
                .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editDependenciesScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE))
                .addContainerGap())
        );
        bodyPanelEditLayout.setVerticalGroup(
            bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelEditLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(jLabel5)
                .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bodyPanelEditLayout.createSequentialGroup()
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(editStudyTaskName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(bodyPanelEditLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(editNameLabel)))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(bodyPanelEditLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(editTaskTypeLabel))
                            .addComponent(editTaskTypes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(editTargetLabel)
                            .addComponent(targetSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editProgressTypes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(bodyPanelEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(editDeadlineLabel)
                            .addComponent(editDeadline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(bodyPanelEditLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(editDependenciesScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addGap(58, 58, 58))
        );

        taskEditPanel.add(bodyPanelEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 840, 310));

        mainPanel.add(taskEditPanel, "editPanel");

        ActivityPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bottomPanelEditAct.setBackground(UserController.activeTint);

        editConfirmButton1.setBackground(UserController.activeTint);
        editConfirmButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.check.png"))); // NOI18N
        editConfirmButton1.setBorder(null);
        editConfirmButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editConfirmButton1ActionPerformed(evt);
            }
        });

        cancelButtonEdit1.setBackground(UserController.activeTint);
        cancelButtonEdit1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/View/GUIAssets/appbar.close.png"))); // NOI18N
        cancelButtonEdit1.setBorder(null);
        cancelButtonEdit1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonEdit1ActionPerformed(evt);
            }
        });

        activityConfirmLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        activityConfirmLabel.setForeground(new java.awt.Color(255, 255, 255));
        activityConfirmLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        activityConfirmLabel.setText("Confirm");

        activityCancelLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 12)); // NOI18N
        activityCancelLabel.setForeground(new java.awt.Color(255, 255, 255));
        activityCancelLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        activityCancelLabel.setText("Cancel");

        activityErrorLable.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        activityErrorLable.setForeground(new java.awt.Color(255, 255, 255));
        activityErrorLable.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        activityErrorLable.setText("**ERROR TEXT GOES HERE**");

        javax.swing.GroupLayout bottomPanelEditActLayout = new javax.swing.GroupLayout(bottomPanelEditAct);
        bottomPanelEditAct.setLayout(bottomPanelEditActLayout);
        bottomPanelEditActLayout.setHorizontalGroup(
            bottomPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelEditActLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bottomPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(editConfirmButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(activityConfirmLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(activityErrorLable, javax.swing.GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(bottomPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cancelButtonEdit1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(activityCancelLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        bottomPanelEditActLayout.setVerticalGroup(
            bottomPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bottomPanelEditActLayout.createSequentialGroup()
                .addGroup(bottomPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bottomPanelEditActLayout.createSequentialGroup()
                        .addComponent(editConfirmButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(activityConfirmLabel))
                    .addGroup(bottomPanelEditActLayout.createSequentialGroup()
                        .addComponent(cancelButtonEdit1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(activityCancelLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(activityErrorLable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        ActivityPanel.add(bottomPanelEditAct, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 840, 90));

        topPanelEditAct.setBackground(UserController.activeTint);

        editActivityTitleLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 48)); // NOI18N
        editActivityTitleLabel.setForeground(new java.awt.Color(255, 255, 255));
        editActivityTitleLabel.setText("Create/Edit Study Activity");

        javax.swing.GroupLayout topPanelEditActLayout = new javax.swing.GroupLayout(topPanelEditAct);
        topPanelEditAct.setLayout(topPanelEditActLayout);
        topPanelEditActLayout.setHorizontalGroup(
            topPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelEditActLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editActivityTitleLabel)
                .addContainerGap(249, Short.MAX_VALUE))
        );
        topPanelEditActLayout.setVerticalGroup(
            topPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelEditActLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(editActivityTitleLabel)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        ActivityPanel.add(topPanelEditAct, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 840, -1));

        bodyPanelEditAct.setBackground(UserController.activeColour);

        activityNameLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        activityNameLabel.setForeground(new java.awt.Color(255, 255, 255));
        activityNameLabel.setText("Activity Name");

        activityTitle.setBackground(UserController.activeTint);
        activityTitle.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        activityTitle.setForeground(new java.awt.Color(255, 255, 255));
        activityTitle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                activityTitleActionPerformed(evt);
            }
        });

        activityProgressType.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        activityProgressType.setForeground(new java.awt.Color(255, 255, 255));
        activityProgressType.setText("type");

        progressSpinner.setFont(new java.awt.Font("Segoe UI Light", 0, 18)); // NOI18N
        progressSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 0, null, 1));

        activityNotes.setColumns(20);
        activityNotes.setRows(5);
        activityNotesScrollPane.setViewportView(activityNotes);

        notePaneLabel.setFont(new java.awt.Font("Segoe UI Light", 1, 36)); // NOI18N
        notePaneLabel.setForeground(new java.awt.Color(255, 255, 255));
        notePaneLabel.setText("Notes");

        activityProgress.setFont(new java.awt.Font("Segoe UI Light", 1, 18)); // NOI18N
        activityProgress.setForeground(new java.awt.Color(255, 255, 255));
        activityProgress.setText("Progress:");

        javax.swing.GroupLayout bodyPanelEditActLayout = new javax.swing.GroupLayout(bodyPanelEditAct);
        bodyPanelEditAct.setLayout(bodyPanelEditActLayout);
        bodyPanelEditActLayout.setHorizontalGroup(
            bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelEditActLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(activityNameLabel)
                    .addComponent(activityProgress))
                .addGap(18, 18, 18)
                .addGroup(bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(bodyPanelEditActLayout.createSequentialGroup()
                        .addComponent(progressSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(activityProgressType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(activityTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(bodyPanelEditActLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(activityNotesScrollPane)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelEditActLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 162, Short.MAX_VALUE)
                        .addComponent(notePaneLabel)
                        .addGap(150, 150, 150))))
        );
        bodyPanelEditActLayout.setVerticalGroup(
            bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bodyPanelEditActLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelEditActLayout.createSequentialGroup()
                        .addGap(0, 54, Short.MAX_VALUE)
                        .addGroup(bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(activityNameLabel)
                            .addComponent(activityTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(51, 51, 51)
                        .addGroup(bodyPanelEditActLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(activityProgressType)
                            .addComponent(progressSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(activityProgress))
                        .addGap(130, 130, 130))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bodyPanelEditActLayout.createSequentialGroup()
                        .addComponent(notePaneLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(activityNotesScrollPane)
                        .addContainerGap())))
        );

        ActivityPanel.add(bodyPanelEditAct, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 840, 310));

        mainPanel.add(ActivityPanel, "activityCard");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(mainPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void newActivityButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newActivityButtonActionPerformed
        CardLayout card = (CardLayout) mainPanel.getLayout();
        card.show(mainPanel, "activityCard");
        modeSet = Mode.ACTIVITYNEWMODE;
        resetActivitypage();
    }//GEN-LAST:event_newActivityButtonActionPerformed

    private void viewCancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewCancelButtonActionPerformed
        parent.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_viewCancelButtonActionPerformed

    private void EditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditActionPerformed
        CardLayout card = (CardLayout) mainPanel.getLayout();
        card.show(mainPanel, "editPanel");
        modeSet = Mode.EDITMODE;
    }//GEN-LAST:event_EditActionPerformed

    private void editTaskTypesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editTaskTypesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editTaskTypesActionPerformed

    private void editConfirmButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editConfirmButtonActionPerformed
        ArrayList<StudyTask> dependencies = new ArrayList<>();
        DefaultTableModel test = (DefaultTableModel) editDependenciesTable.getModel();
        for (int i = 0; i < test.getRowCount(); i++) {
            Boolean b = (Boolean) test.getValueAt(i, 0);
            if (b != null && b) {
                System.out.println(test.getValueAt(i, 1) + "Was true");
                dependencies.add(StudyTaskController.taskList.get(i));
            }
        }
        try {
            if (modeSet == Mode.NEWMODE) {
                StudyTaskController.AddTask(editStudyTaskName.getText(), LocalDate.now(), LocalDate.parse(editDeadline.getText(), FileLoader.formatter), (int) (Integer) targetSpinner.getValue(), dependencies, editTaskTypes.getSelectedIndex(), editProgressTypes.getSelectedIndex());
            } else {
                StudyTaskController.setActiveTaskName(editStudyTaskName.getText());
                StudyTaskController.setActiveTaskDeadline(LocalDate.parse(editDeadline.getText(), FileLoader.formatter));
                StudyTaskController.setActiveTaskDependencies(dependencies);
                StudyTaskController.setActiveTaskTarget((int) (Integer) targetSpinner.getValue());
            }
            parent.setVisible(true);
            parent.updateStudyTaskList();
            this.dispose();
            taskErrorLable.setVisible(false);
        } catch (Exception e) {
            taskErrorLable.setVisible(true);
            taskErrorLable.setText(e.getMessage());
        }
    }//GEN-LAST:event_editConfirmButtonActionPerformed

    private void cancelButtonEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonEditActionPerformed
        parent.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_cancelButtonEditActionPerformed

    private void editStudyTaskNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editStudyTaskNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editStudyTaskNameActionPerformed

    private void editProgressTypesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editProgressTypesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editProgressTypesActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        parent.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void editConfirmButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editConfirmButton1ActionPerformed
        try {
            if (modeSet == Mode.ACTIVITYNEWMODE) {
                StudyActivityController.addActivity(activityTitle.getText(), (int) (Integer) progressSpinner.getValue(), activityNotes.getText());
            }
            if (modeSet == Mode.ACTIVITYEDITMODE) {
                StudyActivityController.setActiveTitle(activityTitle.getText());
                StudyActivityController.setActiveProgress((int) (Integer) progressSpinner.getValue());
                StudyActivityController.setActiveNotes(activityNotes.getText());
            }
            updateActivityList();
            activityErrorLable.setVisible(false);
            switchCard(mainPanel, "viewPanel");
            modeSet = Mode.VIEWMODE;
        } catch (StudyActivityController.InvalidStudyActivityException ex) {
            activityErrorLable.setVisible(true);
            activityErrorLable.setForeground(Color.red);
            activityErrorLable.setText(ex.getMessage());
        }
    }//GEN-LAST:event_editConfirmButton1ActionPerformed

    private void cancelButtonEdit1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonEdit1ActionPerformed
        CardLayout card = (CardLayout) mainPanel.getLayout();
        card.show(mainPanel, "viewPanel");
        modeSet = Mode.VIEWMODE;
    }//GEN-LAST:event_cancelButtonEdit1ActionPerformed

    private void activityTitleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_activityTitleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_activityTitleActionPerformed

    private void viewStudyActivitiesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_viewStudyActivitiesMouseClicked
        if (evt.getClickCount() == 2) {
            int index = viewStudyActivities.getSelectedRow();
            System.out.println("index: " + index);
            StudyActivityController.setActiveActivity(index);
            generateActivityPage();
            switchCard(mainPanel, "activityCard");
            modeSet = Mode.ACTIVITYEDITMODE;
        }
    }//GEN-LAST:event_viewStudyActivitiesMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StudyTaskGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StudyTaskGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StudyTaskGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StudyTaskGUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StudyTaskGUI(null, Mode.VIEWMODE).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel ActivityPanel;
    private javax.swing.JButton Edit;
    private javax.swing.JLabel StudyActivitesTableLabel;
    private javax.swing.JLabel activityCancelLabel;
    private javax.swing.JLabel activityConfirmLabel;
    private javax.swing.JLabel activityErrorLable;
    private javax.swing.JLabel activityNameLabel;
    private javax.swing.JTextArea activityNotes;
    private javax.swing.JScrollPane activityNotesScrollPane;
    private javax.swing.JLabel activityProgress;
    private javax.swing.JLabel activityProgressType;
    private javax.swing.JTextField activityTitle;
    private javax.swing.JPanel bodyPanelEdit;
    private javax.swing.JPanel bodyPanelEditAct;
    private javax.swing.JPanel bodyPanelView;
    private javax.swing.JPanel bottomPanelEdit;
    private javax.swing.JPanel bottomPanelEditAct;
    private javax.swing.JPanel bottomPanelView;
    private javax.swing.JButton cancelButtonEdit;
    private javax.swing.JButton cancelButtonEdit1;
    private javax.swing.JLabel closeView;
    private javax.swing.JLabel editActivityTitleLabel;
    private javax.swing.JLabel editCanelLabel;
    private javax.swing.JButton editConfirmButton;
    private javax.swing.JButton editConfirmButton1;
    private javax.swing.JLabel editConfirmLabel;
    private javax.swing.JFormattedTextField editDeadline;
    private javax.swing.JLabel editDeadlineLabel;
    private javax.swing.JScrollPane editDependenciesScrollPane;
    private javax.swing.JTable editDependenciesTable;
    private javax.swing.JLabel editLabel;
    private javax.swing.JLabel editNameLabel;
    private javax.swing.JComboBox<String> editProgressTypes;
    private javax.swing.JLabel editStudyTaskLabel;
    private javax.swing.JTextField editStudyTaskName;
    private javax.swing.JLabel editTargetLabel;
    private javax.swing.JLabel editTaskTypeLabel;
    private javax.swing.JComboBox<String> editTaskTypes;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JButton newActivityButton;
    private javax.swing.JLabel newLabel;
    private javax.swing.JLabel notePaneLabel;
    private javax.swing.JSpinner progressSpinner;
    private javax.swing.JSpinner targetSpinner;
    private javax.swing.JPanel taskEditPanel;
    private javax.swing.JLabel taskErrorLable;
    private javax.swing.JPanel taskViewPanel;
    private javax.swing.JPanel topPanelEdit;
    private javax.swing.JPanel topPanelEditAct;
    private javax.swing.JPanel topPanelView;
    private javax.swing.JButton viewCancelButton;
    private javax.swing.JLabel viewDeadline;
    private javax.swing.JScrollPane viewDependenciesScrollPane;
    private javax.swing.JTable viewDependenciesTable;
    private javax.swing.JLabel viewProgressType;
    private javax.swing.JTable viewStudyActivities;
    private javax.swing.JScrollPane viewStudyActivitiesScrollPane;
    private javax.swing.JLabel viewStudyTaskName;
    private javax.swing.JLabel viewTaskType;
    // End of variables declaration//GEN-END:variables

    /**
     * Updates the list of activities
     */
    private void updateActivityList() {
        DefaultTableModel test3 = (DefaultTableModel) viewStudyActivities.getModel();
        test3.setRowCount(0);
        for (String[] line : StudyActivityController.getInfoTable()) {
            test3.addRow(new Object[]{line[0], line[1], line[2]});
        }
    }

    /**
     * Switches the current Card
     * @param panel panel to switch
     * @param cardName name of card to be displayed
     */
    private void switchCard(JPanel panel, String cardName) {
        CardLayout card = (CardLayout) panel.getLayout();
        card.show(panel, cardName);
    }

    /**
     * Generates the activity page with info from the active study activity
     */
    private void generateActivityPage() {
        activityTitle.setText(StudyActivityController.getActiveTitle());
        progressSpinner.setValue(Integer.valueOf(StudyActivityController.getActiveProgress()));
        activityProgressType.setText(StudyActivityController.getActiveProgressType());
        activityNotes.setText(StudyActivityController.getActiveNotes());
        activityErrorLable.setVisible(false);
    }

    /**
     * Empties fields
     */
    private void resetActivitypage() {
        activityTitle.setText("");
        progressSpinner.setValue(Integer.valueOf(0));
        activityNotes.setText("");
        activityErrorLable.setVisible(false);
    }
}
