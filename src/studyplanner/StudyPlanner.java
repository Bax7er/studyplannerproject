package studyplanner;

import View.NewGUI.LoginGUI;
import Controller.*;
import Model.User;
import ViewOLD.LoginGui;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author James Baxter
 */
public class StudyPlanner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       /*UserController.addUser("Baxter", "password");
        UserController.addUser("Adam", "password");
        UserController.addUser("Shaun", "password");
        UserController.addUser("Martin", "password");
        try {
            UserController.logIn(0, "password");
        } catch (UserController.InvalidUserDetailException ex) {
            System.out.println(ex.getMessage());
        }/**/
       
       SerializationController.load();
      
        new LoginGUI().setVisible(true);
        SerializationController.save();    
    }
    
}
