package Model;

import java.time.LocalDate;

/**
 * An object that contains basic fields for name, deadline and completion status
 * Designed for sorting multiple different classes by stripping down to their
 * common fields. In hindsight they should have inherited from this class
 *
 * @author James Baxter
 * @version 1
 */
public class SortableDeadlineObject implements Comparable<SortableDeadlineObject> {

    private LocalDate deadline;
    private String name;
    private DeadlineType type;
    private String completionStatus;
    private boolean completed;

    /**
     * Default constructor - sets no fields
     */
    public SortableDeadlineObject() {

    }

    /**
     * 
     * @param name name of this
     * @param deadline deadline for this
     * @param type type of this
     * @param completionStatus string for completion status
     * @param completed boolean for completion status
     */
    public SortableDeadlineObject(String name, LocalDate deadline, DeadlineType type, String completionStatus, boolean completed) {
        this.deadline = deadline;
        this.name = name;
        this.type = type;
        this.completionStatus = completionStatus;
        this.completed = completed;
    }

    /**
     * Enum to track type
     */
    public static enum DeadlineType {
        EXAM("Exam"), COURSEWORK("Coursework"), MILESTONE("Milestone"), STUDYTASK("Study Task");
        public final String title;

        private DeadlineType(String title) {
            this.title = title;
        }
    };

    /**
     * @return the deadline for this
     */
    public LocalDate getDeadline() {
        return deadline;
    }

    /**
     * @param deadline the deadline to set
     */
    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    /**
     * @return the name of this
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public DeadlineType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(DeadlineType type) {
        this.type = type;
    }

    /**
     * @return the completionStatus
     */
    public String getCompletionStatus() {
        return completionStatus;
    }

    /**
     * @param completionStatus the completionStatus to set
     */
    public void setCompletionStatus(String completionStatus) {
        this.completionStatus = completionStatus;
    }

    /**
     * @return boolean completion status
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * @param completed the boolean completion status to set
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    @Override
    public int compareTo(SortableDeadlineObject o) {
        return this.deadline.compareTo(o.deadline);
    }
}
