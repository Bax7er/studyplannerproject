package Model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Used to record tasks for an assignment and track in milestones
 * @author James Baxter
 * @version 1
 */
public class StudyTask implements Serializable {

    public static enum TaskTypes {
        STUDYING("Studying"), PROGRAMMING("Programming"), WRITING("Writing"), OTHER("Other");
        public final String title;

        private TaskTypes(String title) {
            this.title = title;
        }
    };

    public static enum ProgressTypes {
        TIME("Hours"), CHAPTERS("Chapters"), BOOKS("Books"), WRITING("Pages"), OTHER("Units");
        public final String title;

        private ProgressTypes(String title) {
            this.title = title;
        }
    };
    private String taskName;
    private TaskTypes taskType;
    private ProgressTypes progType;
    private LocalDate startDate;
    private LocalDate endDate;
    private int target;
    private int progressCounter;
    private ArrayList<StudyTask> dependencies = new ArrayList<>();
    private ArrayList<StudyActivity> activities = new ArrayList<>();
    private ArrayList<String> notes = new ArrayList<>();
    private boolean completed = false;

    public StudyTask() {
        taskName = "INVALID TASK";
        taskType = null;
        progType = null;
        target = -1;
        progressCounter = 0;
        completed = false;
    }

    /**
     * 
     * @param taskName name for this task
     * @param start start date for this task
     * @param end end date for this task
     * @param target how much of progress type is needed to complete
     * @param dependencies tasks this task depends on
     * @param tasktype category this task falls into
     * @param progType how progress will be tracked for this task
     */
    public StudyTask(String taskName, LocalDate start, LocalDate end, int target, ArrayList<StudyTask> dependencies, TaskTypes tasktype, ProgressTypes progType) {
        this.taskName = taskName;
        this.startDate = start;
        this.endDate = end;
        this.target = target;
        this.progressCounter = 0;
        this.dependencies = dependencies;
        completed = false;
        this.taskType = tasktype;
        this.progType = progType;
    }

    /**
     * 
     * @param other task to check dependency with
     * @return true if this task is dependant on the other, false otherwise
     */
    public boolean dependsOn(StudyTask other) {
        if (dependencies.contains(other)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @return true if this task is complete, false otherwise
     */
    public boolean isComplete() {
        progressCounter = 0;
        for (StudyActivity a : activities) {
            progressCounter += a.getProgress();
        }
        if (progressCounter >= target) {
            completed = true;
        }
        else{
            completed = false;
        }
        return completed;
    }

    /**
     * @return the taskName
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * @param taskName the taskName to set
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * @return the task Type
     */
    public TaskTypes getTaskType() {
        return taskType;
    }

    /**
     * @param taskType the task Type to set
     */
    public void setTaskType(TaskTypes taskType) {
        this.taskType = taskType;
    }

    /**
     * @return the progress Type
     */
    public ProgressTypes getProgType() {
        return progType;
    }

    /**
     * @param progType the progress Type to set
     */
    public void setProgType(ProgressTypes progType) {
        this.progType = progType;
    }

    /**
     * @return the startDate
     */
    public LocalDate getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public LocalDate getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the target
     */
    public int getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(int target) {
        this.target = target;
    }

    /**
     * @return the progress towards the target
     */
    public int getProgressCounter() {
        return progressCounter;
    }

    /**
     * @return the progress towards the target
     */
    public float getProgressDecimal() {
        float prog = progressCounter;
        float targ = target;
        return (prog / target);
    }

    /**
     * @return the progress towards the target
     */
    public float getProgressPercentage() {
        float prog = progressCounter;
        float targ = target;
        return (prog / target) * 100;
    }

    /**
     * @param progressCounter the progressCounter value to set
     */
    public void setProgressCounter(int progressCounter) {
        this.progressCounter = progressCounter;
    }

    /**
     * @return the dependencies
     */
    public ArrayList<StudyTask> getDependencies() {
        return dependencies;
    }

    /**
     * @param dependencies the dependency to add
     */
    public void addDependencies(StudyTask dependency) {
        this.dependencies.add(dependency);
    }

    /**
     * @param dependencies the dependencies to set
     */
    public void setDependencies(ArrayList<StudyTask> dependencies) {
        this.dependencies = dependencies;
    }

    /**
     * @return the notes
     */
    public ArrayList<String> getNotes() {
        return notes;
    }

    /**
     * @param notes the list notes to set
     */
    public void setNotes(ArrayList<String> notes) {
        this.notes = notes;
    }

    /**
     * @param notes the note to add
     */
    public void addNotes(String note) {
        this.notes.add(note);
    }

    /**
     * @param completed state to set, true if task is complete
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    /**
     * @return the activities currently contributing to this task
     */
    public ArrayList<StudyActivity> getActivities() {
        return activities;
    }

    /**
     * @param activities the activities to set
     */
    public void setActivities(ArrayList<StudyActivity> activities) {
        this.activities = activities;
    }

    /**
     * @param activities the activities to add
     */
    public void addActivities(StudyActivity activity) {
        this.activities.add(activity);
    }
}
