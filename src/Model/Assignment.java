package Model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author Adam Carter
 */
public abstract class Assignment implements Comparable<Assignment>, Serializable{
    protected String assignmentName;
    protected LocalDate deadline;
    private ArrayList<StudyTask> tasks = new ArrayList<>();
    private ArrayList<MileStone> milestones= new ArrayList<>();
/**
 * 
 * @return the name of the assignment
 */    
    public String getAssignmentName(){
        return assignmentName;
    }
/**
 * 
 * @return the deadline of the assignment
 */    
    public LocalDate getDeadline(){
        return deadline;
    }
/**
 * 
 * @param assignmentName the name of the assignment
 */    
    public void setAssignmentName(String assignmentName){
        this.assignmentName = assignmentName;
    }
/**
 * 
 * @param deadline the deadline of the assignment
 */    
    public void setDeadline(LocalDate deadline){
        this.deadline = deadline;
    }

    /**
     * @return the tasks
     */
    public ArrayList<StudyTask> getTasks() {
        return tasks;
    }

    /**
     * @param tasks the tasks to set
     */
    public void setTasks(ArrayList<StudyTask> tasks) {
        this.tasks = tasks;
    }

    /**
     * @return the milestones
     */
    public ArrayList<MileStone> getMilestones() {
        return milestones;
    }

    /**
     * @param milestones the milestones to set
     */
    public void setMilestones(ArrayList<MileStone> milestones) {
        this.milestones = milestones;
    }
    
    public void addTask(StudyTask task){
        tasks.add(task);
    }
    
    public void addMilestone(MileStone milestone){
        milestones.add(milestone);
    }
    
    public void removeTask(StudyTask task){
        tasks.remove(task);
    }
    
    public void removeTask(int index){
        tasks.remove(index);
    }
    
    public void removeMilestone(MileStone milestone){
        milestones.remove(milestone);
    }

    public void removeMilestone(int index){
        milestones.remove(index);
    }
    
}
