package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A class to model a user in the study planner system
 *
 * @date 07/04/2017
 * @author James Baxter
 * @version 1
 */
public class User implements Serializable{

    private String UserName;
    private int HashedPassword;
    private ArrayList<SemesterProfile> profiles;
    private int userID;
    private static int userCount = 0;
    private int colourChoice =3;
    /**
     * Creates a user object given no arguments. This should not occur. Username
     * is set to an error user and password is set to blank
     */
    public User() {
        this.UserName = "**ERROR USER**";
        this.HashedPassword = 0;
        this.userID = userCount;
        userCount++;
        profiles = new ArrayList<SemesterProfile>();
    }

    /**
     * Creates a user object given a username and Hashed password
     *
     * @param userName Name to use
     * @param HashedPassword Hashed password to use
     */
    public User(String userName, int HashedPassword) {
        this.UserName = userName;
        this.HashedPassword = HashedPassword;
        this.userID = userCount;
        userCount++;
        profiles = new ArrayList<SemesterProfile>();
    }

    /**
     * 
     * @return Username of this user
     */
    public String getUserName() {
        return this.UserName;
    }

    /**
     * 
     * @return Hashed Password of this user
     */
    public int getPassword() {
        return this.HashedPassword;
    }

    /**
     * 
     * @param UserName new username for this user
     */
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    /**
     * 
     * @param password new password(not hashed) for this user
     */
    public void setPassword(String password) {
        this.HashedPassword = password.hashCode();
    }
    /**
     * 
     * @param password new password for this user
     */
    public void setPassword(int password) {
        this.HashedPassword = password;
    }

    /**
     * Adds a semester profile to this user
     * @param semester semesterProfile to add
     */
    public void addSemesterProfile(SemesterProfile semester) {
        profiles.add(semester);
    }

    /**
     * Removes a semester profile from this user
     * @param position index of profile to remove
     */
    public void removeSemesterProfile(int position) {
        profiles.remove(position);
    }

    /**
     * 
     * @return number of semester profiles this user has
     */
    public int countSemesterProfiles() {
        return profiles.size();
    }
     /**
     * 
     * @return number of semester profiles this user has
     */
    public SemesterProfile getSemesterProfile(int i) {
        return profiles.get(i);
    }
    
    public ArrayList<SemesterProfile> getSemesterProfiles() {
        return profiles;
    }
    
    public int getColourChoice(){
        return colourChoice;
    }
    /**
     * 
     * @param c the position of the colour in the colour array
     */
    public void setColourChoice(int c){
        colourChoice = c;
    }
}
