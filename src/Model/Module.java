package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Adam Carter
 */
public class Module implements Serializable{
    private String moduleCode;
    private String moduleName;
    private int moduleCredit;
    private ArrayList<Exam> exams = new ArrayList <>();
    private ArrayList<Coursework> coursework = new ArrayList <>();
/**
 * 
 * @param moduleCode the unique identifier for a module
 * @param moduleName the name given to the module
 * @param moduleCredit how much this module is credited
 */
    public Module(String moduleCode, String moduleName, int moduleCredit){
        this.moduleCode = moduleCode;
        this.moduleName = moduleName;
        this.moduleCredit = moduleCredit;
    }
/**
 * 
 * @return the unique identifier for the module
 */
    public String getModuleCode(){
        return moduleCode;
    }
/**
 * 
 * @return the name given to the module
 */    
    public String getModuleName(){
        return moduleName;
    }
/**
 * 
 * @return how much this module is credited
 */    
    public int getModuleCredit(){
        return moduleCredit;
    }
/**
 * 
 * @param moduleCode the unique identifier for the module
 */    
    public void setModuleCode(String moduleCode){
        this.moduleCode = moduleCode;
    }
/**
 * 
 * @param moduleName the name given to the module
 */    
    public void setModuleName(String moduleName){
        this.moduleName = moduleName;
    }
/**
 * 
 * @param moduleCredit how much the module is credited
 */    
    public void setModuleCredit(int moduleCredit){
        this.moduleCredit = moduleCredit;
    }
/**
 * 
 * @param e the exam to be added to the exams list
 * @return true if added false if not
 */    
    public boolean addExam(Exam e){
        if(exams.add(e)== true){
            return true;
        }
        else{
            return false;
        }
    }
/**
 * 
 * @param c the coursework to be added to the list
 * @return true if added false if not
 */    
    public boolean addCoursework(Coursework c){
        if(coursework.add(c)== true){
            return true;
        }
        else{
            return false;
        }
    }
/**
 * 
 * @param index the index of the exam to be found
 * @return the exam which has been found
 */    
    public Exam getExam(int index){
        return exams.get(index);
    }
    
/**
 * 
 * @return All exams for this module
 */    
    public ArrayList<Exam> getExams(){
        return exams;
    }
/**
 * 
 * @param index the index of the coursework to be found
 * @return the coursework which has been found
 */    
    public Coursework getCoursework(int index){
        return coursework.get(index);
    }
/**
 * 
 * @return All Coursework for this module
 */    
    public ArrayList<Coursework> getCoursework(){
        return coursework;
    }
/**
 * 
 * @param e the exam to be removed
 */   
    public void removeExam(Exam e){
        exams.remove(e);
    }
/**
 * 
 * @param index the index of the exam to be removed
 */    
    public void removeExam(int index){
        exams.remove(index);
    }
/**
 * 
 * @param c the coursework to be removed
 */    
    public void removeCoursework(Coursework c){
        coursework.remove(c);
    }
/**
 * 
 * @param index the index of the coursework to be removed
 */    
    public void removeCoureswork(int index){
        coursework.remove(index);
    }
}
