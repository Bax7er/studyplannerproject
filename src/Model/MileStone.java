package Model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

/**
 * A class to model a milestone
 * Milestone progress and completion is tracked by the progress or completion
 * of the StudyTasks it references
 * @author James Baxter
 * @version 1
 */
public class MileStone implements Comparable<MileStone>, Serializable{

    // Name of this Milestone
    private String milestoneName;
    
    //Deadline - when this milestone should be competed by
    private LocalDate  deadLine;
    
    //Tasks this milestone tracks
    private ArrayList<StudyTask> tasks = new ArrayList<>();
    
    //Whether this milestone is completed
    private boolean completed;

    /**
     * Default constructor. If used, values should be changed immediately afterwards
     */
    public MileStone() {
        milestoneName = "INVALID MILESTONE";
        deadLine = null;
        completed = false;
    }

    /**
     * 
     * @param name Name of this Milestone
     * @param deadline expected completion date
     */
    public MileStone(String name, LocalDate  deadline) {
        this.milestoneName = name;
        this.deadLine = deadline;
        completed = false;
    }

    /**
     * 
     * @param name Name of this Milestone
     * @param deadline expected completion date
     * @param tasks tasks this milestone consists of
     */
    public MileStone(String name, LocalDate  deadline, ArrayList<StudyTask> tasks) {
        this.milestoneName = name;
        this.deadLine = deadline;
        this.tasks = tasks;
        completed = false;
    }

    /**
     * @return the milestoneName
     */
    public String getMilestoneName() {
        return milestoneName;
    }

    /**
     * @param milestoneName the milestoneName to set
     */
    public void setMilestoneName(String milestoneName) {
        this.milestoneName = milestoneName;
    }

    /**
     * @return the completion deadLine
     */
    public LocalDate  getDeadLine() {
        return deadLine;
    }

    /**
     * @param deadLine the deadLine to set
     */
    public void setDeadLine(LocalDate deadLine) {
        this.deadLine = deadLine;
    }

    /**
     * @return the tasks this milestone tracks
     */
    public ArrayList<StudyTask> getTasks() {
        return tasks;
    }

    /**
     * @param tasks the tasks to set
     */
    public void setTasks(ArrayList<StudyTask> tasks) {
        this.tasks = tasks;
    }
    
    /**
     * @param tasks the tasks to set
     */
    public void addTask(StudyTask task) {
        this.tasks.add(task);
    }

    /**
     * @return true if all tasks within this milestone are completed, false otherwise
     */
    public boolean isCompleted() {
        if(!completed){
            for(StudyTask task : this.tasks){
                if(!task.isComplete()){
                    completed = false;
                }
            }
        }
        return this.completed;
    }

    /**
     * @param completed true if all tasks completed, false otherwise
     */
    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    /**
     * Compares the deadlines of two milestones
     * @param o the milestone to compare to
     * @return 0 if same, -1 if before, 1 if after
     */
    @Override
    public int compareTo(MileStone o) {
        return deadLine.compareTo(o.getDeadLine());
    }
    
}
