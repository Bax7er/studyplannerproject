package Model;

import java.io.Serializable;
import java.time.LocalDate;
/**
 *
 * @author Adam Carter
 */
public class Coursework extends Assignment implements Serializable{
    private LocalDate  dateSet;
/**
 * 
 * @param assignmentName the name of the coursework
 * @param dateSet the date which the coursework was set 
 * @param deadline the deadline of the coursework
 */    
    public Coursework(String assignmentName, LocalDate  dateSet, LocalDate deadline){
        this.assignmentName = assignmentName;
        this.dateSet = dateSet;
        this.deadline = deadline;
    }
/**
 * 
 * @return the date which the coursework is set
 */    
    public LocalDate  getDateSet(){
        return dateSet;
    }
/**
 * 
 * @param dateSet the date which the coursework is set
 */    
    public void setDateSet(LocalDate  dateSet){
        this.dateSet = dateSet;
    }

    @Override
    public int compareTo(Assignment o) {
       return this.deadline.compareTo(o.deadline);
    }
}
