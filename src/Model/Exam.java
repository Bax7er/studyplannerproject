package Model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author Adam Carter
 */
public class Exam extends Assignment implements Serializable{
    private int duration;
/**
 * 
 * @param assignmentName the name of the exam
 * @param deadline the deadline of the exam
 * @param duration the duration of the exam
 */    
    public Exam(String assignmentName, LocalDate  deadline, int duration){
        this.assignmentName = assignmentName;
        this.deadline = deadline;
        this.duration = duration;
    }
/**
 * 
 * @return the duration of the exam
 */    
    public int getDuration(){
        return duration;
    }
/**
 * 
 * @param duration the duration of the exam
 */    
    public void setDuration(int duration){
        this.duration = duration;
    }

    @Override
    public int compareTo(Assignment o) {
       return this.deadline.compareTo(o.deadline);
    }
}
