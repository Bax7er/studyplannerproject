package Model;

import Model.StudyTask.ProgressTypes;
import java.io.Serializable;

/**
 * A class to model study activities - used to track StudyTask progress
 *
 * @author James Baxter
 * @version 1
 */
public class StudyActivity implements Serializable {

    private String description;
    private ProgressTypes progType;
    private int progress;
    private String notes;

    /**
     * Default constructor - sets a warning for the description
     */
    public StudyActivity() {
        description = "INVALID STUDY ACTIVITY";
        progType = null;
        progress = 0;
    }

    /**
     *
     * @param description Name of this task
     * @param type progress type of StudyTask it related to
     * @param progress progress it will contribute
     */
    public StudyActivity(String description, ProgressTypes type, int progress) {
        this.description = description;
        progType = type;
        this.progress = progress;
    }

    /**
     *
     * @param description Name of this task
     * @param type progress type of StudyTask it related to
     * @param progress progress it will contribute
     * @param note notes for this activity
     */
    public StudyActivity(String description, ProgressTypes type, int progress, String note) {
        this.description = description;
        progType = type;
        this.progress = progress;
        this.notes = note;
    }

    /**
     * @return the description/name
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description/name to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the progress Type
     */
    public ProgressTypes getProgType() {
        return progType;
    }

    /**
     * @param progType the progress Type to set
     */
    public void setProgType(ProgressTypes progType) {
        this.progType = progType;
    }

    /**
     * @return the progress counter
     */
    public int getProgress() {
        return progress;
    }

    /**
     * @param progress the progress count to set
     */
    public void setProgress(int progress) {
        this.progress = progress;
    }

    /**
     * @return the notes for this study task
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
