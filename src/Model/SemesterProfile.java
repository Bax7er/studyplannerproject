package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Models a single Semester and acts as container for modules
 * @author James Baxter
 * @version 1
 */
public class SemesterProfile implements Serializable{

    private int year;
    private char semesterPeriod;
    private ArrayList<Module> modules;

    /**
     * Default constructor - fields initialised to invalid values and should be
     * changed once constructed
     */
    public SemesterProfile() {
        year = 0;
        semesterPeriod = '\n';
        modules = new ArrayList<>();
    }

    /**
     * Constructor to initialise basic fields for this profile - not validated 
     * @param year starting year for this semester
     * @param semesterPeriod the semester period (A or B)
     */
    public SemesterProfile(int year, char semesterPeriod) {
        this.year = year;
        this.semesterPeriod = semesterPeriod;
        modules = new ArrayList<>();
    }

    /**
     * Gets the starting year for this semester
     *
     * @return starting year for this semester
     */
    public int getYear() {
        return year;
    }

    /**
     * Gets the semester period (A or B)
     *
     * @return Semester period of this profile
     */
    public char getSemesterPeriod() {
        return semesterPeriod;
    }

    /**
     * Sets the starting year for this semester
     *
     * @param year starting year
     */
    public void setYear(int startyear) {
        year = startyear;
    }

    /**
     * Sets the semester period for this semester
     *
     * @param char Semester period of this profile
     */
    public void setSemesterPeriod(char period) {
        semesterPeriod = period;
    }

    /**
     * Adds module to this Semester Profile
     *
     * @param m Module to add
     * @return true if successful, false otherwise
     */
    public boolean addModule(Module m) {
        return modules.add(m);
    }

    /**
     * Removes module to this Semester Profile
     *
     * @param m Module to remove
     * @return true if successful, false otherwise
     */
    public boolean removeModule(Module m) {
        return modules.remove(m);
    }

    /**
     * Removes module to this Semester Profile
     *
     * @param m index of module to remove
     * @return true if successful, false otherwise
     */
    public boolean removeModule(int i) {
        return (modules.remove(i) != null);
    }
    
    /**
     * 
     * @return arrayList of modules for this semester profile 
     */
    public ArrayList<Module> getModules(){
        return modules;
    }
    
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder(26);
         str.append("Year: ");
            str.append(this.year);
            str.append('-');
            str.append(this.year + 1);
            str.append(" Period: ");
            str.append(this.semesterPeriod);
            return str.toString();
    }
}
