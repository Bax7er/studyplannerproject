/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.String;
import java.io.File;
import java.net.URL;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Shaun
 */
public class FileLoaderTest {
    
    public FileLoaderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws UserController.InvalidUserDetailException {
        UserController.addUser("user", "password");
        UserController.logIn(0, "password");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFileType method, of class FileLoader.
     */
    @Test
    public void testGetFileType() {
        System.out.println("getFileType");
        String expResult = "semester file";
        String result = FileLoader.getFileType();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getFileExtention method, of class FileLoader.
     */
    @Test
    public void testGetFileExtention() {
        System.out.println("getFileExtention");
        String expResult = "sf";
        String result = FileLoader.getFileExtention();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
    }


    /**
     * Test of getYear method, of class FileLoader.
     */
    @Test
    public void testGetYear() throws Exception {
        System.out.println("getYear");
        URL url = getClass().getResource("testCaseFile.sf");
        File file = new File(url.getPath());
        int expResult = 2016;
        int result = FileLoader.getYear(file);
        assertEquals(expResult, result);
        if(result != expResult){
            fail();
        }
    }

    /**
     * Test of loadFile method, of class FileLoader.
     * file closed not working
     */
    @Test
    public void testLoadFile() throws Exception {
        System.out.println("loadFile");
        URL url = getClass().getResource("testCaseFile.sf");
        File file = new File(url.getPath());
        boolean expResult = true;
        boolean result = FileLoader.loadFile(file);
        assertEquals(expResult, result);
        if(expResult != result){
            fail();
        }
    }

    /**
     * Test of upDatedFile method, of class FileLoader.
     */
    @Test
    public void testUpDatedFile() throws Exception {
        System.out.println("upDatedFile");
        URL url = getClass().getResource("updatedTestCaseFile.sf");
        File file = new File(url.getPath());
        boolean expResult = true;
        boolean result = FileLoader.upDatedFile(file);
        assertEquals(expResult, result);
        if(expResult != result){
            fail();
        }
    }
    
}
