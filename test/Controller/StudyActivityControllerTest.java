/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Controller.FileLoader.formatter;
import Model.StudyActivity;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Controller.StudyTaskController;
import Model.StudyTask;
import Model.StudyTask.ProgressTypes;
import java.time.LocalDate;
/**
 *
 * @author Shaun
 */
public class StudyActivityControllerTest {
    
    public StudyActivityControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws StudyTaskController.InvalidStudyTaskException {
        AssignmentController.addCoursework("Software Engineering", LocalDate.parse("13/03/2017", formatter), LocalDate.parse("17/05/2017", formatter));
        StudyTaskController.AddTask("Make test Classes",LocalDate.parse("13/03/2017", formatter), LocalDate.parse("13/05/2017", formatter), 10,new ArrayList<>(), 1, 3);
        StudyTaskController.setActiveTask(0);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ArrayList<StudyActivity> activities = new ArrayList<>();
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,2, "Do a bit of Porgraming"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,3, "Write 3 classes"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,4, "Write four more classes"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,3, "Do more Porgraming"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,5, "Do even more Porgraming"));
        StudyActivityController.loadActivityList(activities);
    
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of loadActivityList method, of class StudyActivityController.
     */
    @Test
    public void testLoadActivityList() {
        System.out.println("loadActivityList");
        ArrayList<StudyActivity> activities = new ArrayList<>();
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,4, "Do some Porgraming"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,1, "Write a bit classes"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,2, "Write two more classes"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,4, "Do more Porgraming"));
        activities.add(new StudyActivity("Porgraming",ProgressTypes.WRITING,3, "Do even more Porgraming"));
        StudyActivityController.loadActivityList(activities);
        
        ArrayList<String[]> expResult = new ArrayList<>();
        
        expResult.add(new String[]{"Porgraming","4", "Do some Porgraming"});
        expResult.add(new String[]{"Porgraming","1", "Write a bit classes"});
        expResult.add(new String[]{"Porgraming","2", "Write two more classes"});
        expResult.add(new String[]{"Porgraming","4", "Do more Porgraming"});
        expResult.add(new String[]{"Porgraming","3", "Do even more Porgraming"});
        ArrayList result = StudyActivityController.getInfoTable();
        
        for(int x = 0;x < expResult.size(); x++){
            for(int y = 0;y < 3; y++){
                String[] temp1 = (String[])result.get(x);
                String[] temp2 = (String[])expResult.get(x);
                if(!temp1[y].equals(temp2[y])){
                    fail();
                } 
            }
        }
    }

    /**
     * Test of addActivity method, of class StudyActivityController.
     */
    @Test
    public void testAddActivity() throws Exception {
        System.out.println("addActivity");
        String description = "Reading";
        int progress = 10;
        String notes = "Read 10 pages";
        
        StudyActivityController.addActivity(description, progress, notes);
        StudyActivityController.setActiveActivity(StudyActivityController.activityList.size()-1);
        
        if(!description.equals(StudyActivityController.getActiveTitle())){
            fail();
        }
        if(progress != StudyActivityController.getActiveProgress()){
            fail();
        }
        if(!notes.equals(StudyActivityController.getActiveNotes())){
            fail();
        }
    }

    /**
     * Test of getInfoTable method, of class StudyActivityController.
     */
    @Test
    public void testGetInfoTable() {
        System.out.println("getInfoTable");
        ArrayList<String[]> expResult = new ArrayList<>();
        
        expResult.add(new String[]{"Porgraming","2", "Do a bit of Porgraming"});
        expResult.add(new String[]{"Porgraming","3", "Write 3 classes"});
        expResult.add(new String[]{"Porgraming","4", "Write four more classes"});
        expResult.add(new String[]{"Porgraming","3", "Do more Porgraming"});
        expResult.add(new String[]{"Porgraming","5", "Do even more Porgraming"});
        ArrayList result = StudyActivityController.getInfoTable();
        //assertEquals(expResult, result);
        for(int x = 0;x < expResult.size(); x++){
            for(int y = 0;y < 3; y++){
                String[] temp1 = (String[])result.get(x);
                String[] temp2 = (String[])expResult.get(x);
                if(!temp1[y].equals(temp2[y])){
                    fail();
                } 
            }
        }
    }

    /**
     * Test of setActiveActivity method, of class StudyActivityController.
     */
    @Test
    public void testSetActiveActivity() {
        System.out.println("setActiveActivity");
        int index = 2;
        String expResult = "Write four more classes";
        StudyActivityController.setActiveActivity(index);
        String result = StudyActivityController.getActiveNotes();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getActiveTitle method, of class StudyActivityController.
     */
    @Test
    public void testGetActiveTitle() {
        System.out.println("getActiveTitle");
        String expResult = "Porgraming";
        String result = StudyActivityController.getActiveTitle();
        assertEquals(expResult, result);
        if(result != expResult){
            fail();
        }
    }

    /**
     * Test of getActiveProgress method, of class StudyActivityController.
     */
    @Test
    public void testGetActiveProgress() {
        System.out.println("getActiveProgress");
        int expResult = 2;
        int result = StudyActivityController.getActiveProgress();
        assertEquals(expResult, result);
        if(result != expResult){
            fail();
        }
    }

    /**
     * Test of getActiveNotes method, of class StudyActivityController.
     */
    @Test
    public void testGetActiveNotes() {
        System.out.println("getActiveNotes");
        String expResult = "Do a bit of Porgraming";
        String result = StudyActivityController.getActiveNotes();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getActiveProgressType method, of class StudyActivityController.
     */
    @Test
    public void testGetActiveProgressType() {
        System.out.println("getActiveProgressType");
        String expResult = "Pages";
        String result = StudyActivityController.getActiveProgressType();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of setActiveTitle method, of class StudyActivityController.
     */
    @Test
    public void testSetActiveTitle() throws Exception {
        System.out.println("setActiveTitle");
        String text = "Test Title";
        StudyActivityController.setActiveTitle(text);
        String result = StudyActivityController.getActiveTitle();
        if(!result.equals(text)){
            fail();
        }
    }

    /**
     * Test of setActiveProgress method, of class StudyActivityController.
     */
    @Test
    public void testSetActiveProgress() throws Exception {
        System.out.println("setActiveProgress");
        int i = 56;
        StudyActivityController.setActiveProgress(i);
        int result = StudyActivityController.getActiveProgress();
        if(result != i){
            fail();
        }
    }

    /**
     * Test of setActiveNotes method, of class StudyActivityController.
     */
    @Test
    public void testSetActiveNotes() {
        System.out.println("setActiveNotes");
        String text = "Note Test";
        StudyActivityController.setActiveNotes(text);
        String result = StudyActivityController.getActiveNotes();
        if(!result.equals(text)){
            fail();
        }
        
    }
    
}
