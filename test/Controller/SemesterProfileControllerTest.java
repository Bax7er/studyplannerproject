/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.SemesterProfile;
import Model.User;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Baxter
 */
public class SemesterProfileControllerTest {

    public SemesterProfileControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        User TestUser = new User("Test", "password".hashCode());
        TestUser.addSemesterProfile(new SemesterProfile(2016, 'A'));
        TestUser.addSemesterProfile(new SemesterProfile(2016, 'B'));
        TestUser.addSemesterProfile(new SemesterProfile(2017, 'A'));
        TestUser.addSemesterProfile(new SemesterProfile(2017, 'B'));
        UserController.addUser(TestUser);
        try {
            UserController.logIn(0, "password");
        } catch (UserController.InvalidUserDetailException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @After
    public void tearDown() {
        UserController.clear();
    }

    /**
     * Test of LoadProfiles method, of class SemesterProfileController.
     */
    @Test
    public void testLoadProfiles_ArrayList() {
        System.out.println("LoadProfiles");
        ArrayList<SemesterProfile> profiles = new ArrayList<>();
        SemesterProfile expResult = new SemesterProfile(2018, 'A');
        profiles.add(expResult);
        profiles.add(new SemesterProfile(2018, 'B'));
         
        SemesterProfileController.LoadProfiles(profiles);
        
        assertEquals(expResult.toString(), SemesterProfileController.activeProfile.toString());
    }

    /**
     * Test of LoadProfiles method, of class SemesterProfileController.
     */
    @Test
    public void testLoadProfiles_0args() {
        System.out.println("LoadProfiles");
        SemesterProfileController.LoadProfiles();
        SemesterProfile expResult = new SemesterProfile(2016, 'A');
        assertEquals(expResult.toString(), SemesterProfileController.activeProfile.toString());
    }

    /**
     * Test of newProfile method, of class SemesterProfileController.
     */
    @Test
    public void testNewProfile(){
        System.out.println("newProfile");
        int year = 2000;
        char semesterPeriod = 'C';
        boolean caught = false;
        try {
            SemesterProfileController.newProfile(year, semesterPeriod);
        } catch (SemesterProfileController.InvalidSemesterProfileException ex) {
            caught = true;
        }
        if(!caught){
            fail();
        }
        year = 2015;
        caught = false;
        try {
            SemesterProfileController.newProfile(year, semesterPeriod);
        } catch (SemesterProfileController.InvalidSemesterProfileException ex) {
            caught = true;
        }
        if(!caught){
            fail();
        }
        semesterPeriod = 'B';
         try {
            SemesterProfileController.newProfile(year, semesterPeriod);
        } catch (SemesterProfileController.InvalidSemesterProfileException ex) {
            fail();
        }
         assertTrue(true);
    }

    /**
     * Test of setActiveProfile method, of class SemesterProfileController.
     */
    @Test
    public void testSetActiveProfile() {
        System.out.println("setActiveProfile");
        int i = 3;
        SemesterProfileController.setActiveProfile(i);
        SemesterProfile expResult = new SemesterProfile(2017, 'B');
        assertEquals(expResult.toString(), SemesterProfileController.activeProfile.toString());
    }

    /**
     * Test of listProfileNames method, of class SemesterProfileController.
     */
    @Test
    public void testListProfileNames() {
        System.out.println("listProfileNames");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add(new SemesterProfile(2016, 'A').toString());
        expResult.add(new SemesterProfile(2016, 'B').toString());
        expResult.add(new SemesterProfile(2017, 'A').toString());
        expResult.add(new SemesterProfile(2017, 'B').toString());
        ArrayList<String> result = SemesterProfileController.listProfileNames();
        assertEquals(expResult, result);
    }

}
