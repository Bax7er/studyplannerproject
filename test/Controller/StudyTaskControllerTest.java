/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Controller.FileLoader.formatter;
import Model.Assignment;
import Model.Coursework;
import Model.StudyTask;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Shaun
 */
public class StudyTaskControllerTest {
    
    public StudyTaskControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass(){
        AssignmentController.addCoursework("Software Engineering", LocalDate.parse("13/03/2017", formatter), LocalDate.parse("17/05/2017", formatter));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws StudyTaskController.InvalidStudyTaskException {
        StudyTaskController.AddTask("Make test Classes",LocalDate.parse("13/03/2017", formatter), LocalDate.parse("13/05/2017", formatter), 10,new ArrayList<>(), 1, 3);
        StudyTaskController.AddTask("Write Report",LocalDate.parse("20/03/2017", formatter), LocalDate.parse("04/04/2017", formatter), 5,new ArrayList<>(), 1, 3);
    }
    
    @After
    public void tearDown() {
        StudyTaskController.taskList.clear();
    }

    /**
     * Test of AddTask method, of class StudyTaskController.
     */
    @Test
    public void testAddTask_8args() {
        System.out.println("AddTask");
        Assignment assignment = new Coursework("test", LocalDate.parse("21/01/2017", formatter), LocalDate.parse("21/04/2017", formatter));
        String taskName = "add Task Test";
        LocalDate start = LocalDate.parse("21/02/2017", formatter);
        LocalDate end = LocalDate.parse("14/03/2017", formatter);
        int target = 5;
        ArrayList<StudyTask> dependencies = null;
        int tasktype = 1;
        int progType = 3;
        StudyTaskController.AddTask(assignment, taskName, start, end, target, dependencies, tasktype, progType);
        
        String resultName = assignment.getTasks().get(0).getTaskName();
        LocalDate resultDateEnd = assignment.getTasks().get(0).getEndDate();
        int resultTarget = assignment.getTasks().get(0).getTarget();
        if(!resultName.equals(taskName)){
            fail();
        }
        if(!resultDateEnd.equals(LocalDate.parse("14/03/2017", formatter))){
            fail();
        }
        if(resultTarget != target){
            fail();
        }
    }

    /**
     * Test of AddTask method, of class StudyTaskController.
     */
    @Test
    public void testAddTask_7args() throws Exception {
        System.out.println("AddTask");
        String taskName = "add Task Test";
        LocalDate start = LocalDate.parse("21/02/2017", formatter);
        LocalDate end = LocalDate.parse("14/03/2017", formatter);
        int target = 5;
        ArrayList<StudyTask> dependencies = new ArrayList<>();
        int tasktype = 1;
        int progType = 3;
        StudyTaskController.AddTask(taskName, start, end, target, dependencies, tasktype, progType);
        StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
        String resultName = StudyTaskController.getActiveTaskTitle();
        String resultDateEnd = StudyTaskController.getActiveDeadline();
        int resultTarget = StudyTaskController.getActiveTarget();
        if(!resultName.equals(taskName)){
            fail();
        }
        if(!resultDateEnd.equals("14/03/2017")){
            fail();
        }
        if(resultTarget != target){
            fail();
        }
    }

    /**
     * Test of setActiveTask method, of class StudyTaskController.
     */
    @Test
    public void testSetActiveTask() {
        System.out.println("setActiveTask");
        int index = 1;
        StudyTaskController.setActiveTask(index);
        String resultName = StudyTaskController.getActiveTaskTitle();
        String resultDateEnd = StudyTaskController.getActiveDeadline();
        int resultTarget = StudyTaskController.getActiveTarget();
        if(!resultName.equals("Write Report")){
            fail();
        }
        if(!resultDateEnd.equals("04/04/2017")){
            fail();
        }
        if(resultTarget != 5){
            fail();
        }
    }
    /**
     * Test of getNames method, of class StudyTaskController.
     */
    @Test
    public void testGetNames() {
        System.out.println("getNames");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Make test Classes");
        expResult.add("Write Report");
        
        StudyTaskController.setActiveTask(StudyTaskController.taskList.size()-1);
        ArrayList<String> result = StudyTaskController.getNames();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getTaskTypes method, of class StudyTaskController.
     */
    @Test
    public void testGetTaskTypes() {
        System.out.println("getTaskTypes");
        String expResult[] = new String[4];
        expResult[0] = "Studying";
        expResult[1] = "Programming";
        expResult[2] = "Writing";
        expResult[3] = "Other";
        String[] result = StudyTaskController.getTaskTypes();
        assertArrayEquals(expResult, result);
        for(int x = 1; x <expResult.length;x++){
            if(!expResult[x].equals(result[x])){
                fail();
            }
        }
    }

    /**
     * Test of getActiveTaskTitle method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveTaskTitle() {
        System.out.println("getActiveTaskTitle");
        String expResult = "Write Report";
        String result = StudyTaskController.getActiveTaskTitle();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
                fail();
        }
    }

    /**
     * Test of getActiveTaskType method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveTaskType() {
        System.out.println("getActiveTaskType");
        String expResult = "Programming";
        String result = StudyTaskController.getActiveTaskType();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
                fail();
        }
    }

    /**
     * Test of getActiveDeadline method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveDeadline() {
        System.out.println("getActiveDeadline");
        String expResult = "14/03/2017";
        String result = StudyTaskController.getActiveDeadline();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
                fail();
        }
    }

    /**
     * Test of getActiveTaskTypeOrdinal method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveTaskTypeOrdinal() {
        System.out.println("getActiveTaskTypeOrdinal");
        int expResult = 1;
        int result = StudyTaskController.getActiveTaskTypeOrdinal();
        assertEquals(expResult, result);
        if(result != expResult){
            fail();
        }
    }

    /**
     * Test of setActiveTaskName method, of class StudyTaskController.
     */
    @Test
    public void testSetActiveTaskName() {
        System.out.println("setActiveTaskName");
        String text = "Test set name";
        StudyTaskController.setActiveTask(0);
        StudyTaskController.setActiveTaskName(text);
        String result = StudyTaskController.getActiveTaskTitle();
        if(!text.equals(result)){
                fail();
        }
    }

    /**
     * Test of setActiveTaskDeadline method, of class StudyTaskController.
     */
    @Test
    public void testSetActiveTaskDeadline() {
        System.out.println("setActiveTaskDeadline");
        LocalDate parse = LocalDate.parse("25/04/2017", formatter);
        StudyTaskController.setActiveTaskDeadline(parse);
        String result = StudyTaskController.getActiveDeadline();
        if(!result.equals("25/04/2017")){
                fail();
        }
    }

    /**
     * Test of setActiveTaskDependencies method, of class StudyTaskController.
     */
    @Test
    public void testSetActiveTaskDependencies() {
        System.out.println("setActiveTaskDependencies");
        ArrayList<StudyTask> dependencies = new ArrayList<>();
        dependencies.add(StudyTaskController.taskList.get(0));
        StudyTaskController.setActiveTaskDependencies(dependencies);
        ArrayList<StudyTask> result = StudyTaskController.activeTask.getDependencies();
        if(!result.equals(dependencies)){
            fail();
        }
    }

    /**
     * Test of getProgressTypes method, of class StudyTaskController.
     */
    @Test
    public void testGetProgressTypes() {
        System.out.println("getProgressTypes");
        String expResult[] = new String[5];
        expResult[0] = "Hours";
        expResult[1] = "Chapters";
        expResult[2] = "Books";
        expResult[3] = "Pages";
        expResult[4] = "Units";
        String[] result = StudyTaskController.getProgressTypes();
        assertArrayEquals(expResult, result);
        for(int x = 0; x < expResult.length; x++){
            if(!expResult[x].equals(result[x])){
                fail();
            }
        }
        
    }

    /**
     * Test of getActiveTarget method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveTarget() {
        System.out.println("getActiveTarget");
        int expResult = 5;
        int result = StudyTaskController.getActiveTarget();
        assertEquals(expResult, result);
        if(expResult != result){
            fail();
        }
    }

    /**
     * Test of getActiveProgressType method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveProgressType() {
        System.out.println("getActiveProgressType");
        String expResult = "Pages";
        String result = StudyTaskController.getActiveProgressType();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getActiveProgressTypeOrdinal method, of class StudyTaskController.
     */
    @Test
    public void testGetActiveProgressTypeOrdinal() {
        System.out.println("getActiveProgressTypeOrdinal");
        int expResult = 3;
        int result = StudyTaskController.getActiveProgressTypeOrdinal();
        assertEquals(expResult, result);
        if(expResult != result){
            fail();
        }
    }

    /**
     * Test of setActiveTaskTarget method, of class StudyTaskController.
     */
    @Test
    public void testSetActiveTaskTarget() {
        System.out.println("setActiveTaskTarget");
        int target = 30;
        StudyTaskController.setActiveTaskTarget(target);
        int result = StudyTaskController.getActiveTarget();
        if(target != result){
            fail();
        }
    }
    
}
