/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Controller.FileLoader.formatter;
import Model.SortableDeadlineObject;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Shaun
 */
public class DeadlineObjectsControllerTest {
    
    public DeadlineObjectsControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
        
        
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ArrayList<SortableDeadlineObject> deadlineObjects = new ArrayList<>();
        deadlineObjects.add(new SortableDeadlineObject("DeadLine1", LocalDate.parse("17/03/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        deadlineObjects.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        deadlineObjects.add(new SortableDeadlineObject("DeadLine3", LocalDate.parse("21/02/2017", formatter), SortableDeadlineObject.DeadlineType.MILESTONE, "", true));
        DeadlineObjectsController.deadlineObjects = deadlineObjects;
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of sortDeadlines method, of class DeadlineObjectsController.
     */
    @Test
    public void testSortDeadlines() {
        System.out.println("sortDeadlines");
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        expResult.add(new SortableDeadlineObject("DeadLine1", LocalDate.parse("17/03/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        expResult.add(new SortableDeadlineObject("DeadLine3", LocalDate.parse("21/02/2017", formatter), SortableDeadlineObject.DeadlineType.MILESTONE, "", true));
        
        LocalDate date = LocalDate.parse("10/03/2017", formatter);
        DeadlineObjectsController.sortDeadlines();
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();  
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();  
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();  
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();  
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();  
            }
        }
    }

    /**
     * Test of getAfter method, of class DeadlineObjectsController.
     */
    @Test
    public void testGetAfter() {
        System.out.println("getAfter");
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine1", LocalDate.parse("17/03/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        expResult.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        LocalDate date = LocalDate.parse("10/03/2017", formatter);
        DeadlineObjectsController.getAfter(date);
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();  
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();  
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();  
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();  
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();  
            }
        }
        
    }

    /**
     * Test of getBefore method, of class DeadlineObjectsController.
     */
    @Test
    public void testGetBefore() {
        System.out.println("getBefore");
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine1", LocalDate.parse("17/03/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        expResult.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        LocalDate date = LocalDate.parse("20/03/2017", formatter);
        DeadlineObjectsController.getBefore(date);
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();  
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();  
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();  
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();  
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();  
            }
        }
    }

    /**
     * Test of getExactly method, of class DeadlineObjectsController.
     */
    @Test
    public void testGetExactly() {
        System.out.println("getExactly");
        LocalDate date = LocalDate.parse("30/04/2017", formatter);
        DeadlineObjectsController.getExactly(date);
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();
            }
        }
    }

    /**
     * Test of saveType method, of class DeadlineObjectsController.
     */
    @Test
    public void testSaveType() {
        System.out.println("saveType");
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine3", LocalDate.parse("21/02/2017", formatter), SortableDeadlineObject.DeadlineType.MILESTONE, "", true));
        
        SortableDeadlineObject.DeadlineType type = SortableDeadlineObject.DeadlineType.MILESTONE;
        DeadlineObjectsController.saveType(type);
        
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();  
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();  
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();  
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();  
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();  
            }
        }
    }

    /**
     * Test of removeType method, of class DeadlineObjectsController.
     */
    @Test
    public void testRemoveType() {
        System.out.println("removeType");
        SortableDeadlineObject.DeadlineType type = SortableDeadlineObject.DeadlineType.MILESTONE;
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine1", LocalDate.parse("17/03/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        expResult.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        
        DeadlineObjectsController.removeType(type);
        
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();  
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();  
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();  
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();  
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();  
            }
        }
    }

    /**
     * Test of getTable method, of class DeadlineObjectsController.
     */
    @Test
    public void testGetTable() {
        System.out.println("getTable");
        ArrayList<String[]> expResult = new ArrayList<>();
        expResult.add(new String[]{"Coursework","DeadLine1","17/03/2017",""});
        expResult.add(new String[]{"Coursework","DeadLine2","30/04/2017",""});
        expResult.add(new String[]{"Milestone","DeadLine3","21/02/2017",""});
        ArrayList<String[]> result = DeadlineObjectsController.getTable();
        for(int x = 0; x < expResult.size()-1; x++){
            if(!expResult.get(x)[0].equals(result.get(x)[0])){
                fail();
            }
            
            if(!expResult.get(x)[1].equals(result.get(x)[1])){
                fail();
            }
            
            if(!expResult.get(x)[2].equals(result.get(x)[2])){
                fail();
            }
        }
    }

    /**
     * Test of removeCompleted method, of class DeadlineObjectsController.
     */
    @Test
    public void testRemoveCompleted() {
        System.out.println("removeCompleted");
        ArrayList<SortableDeadlineObject> expResult = new ArrayList<>();
        expResult.add(new SortableDeadlineObject("DeadLine1", LocalDate.parse("17/03/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        expResult.add(new SortableDeadlineObject("DeadLine2", LocalDate.parse("30/04/2017", formatter), SortableDeadlineObject.DeadlineType.COURSEWORK, "", false));
        
        DeadlineObjectsController.removeCompleted();
        
        for(int x = 0; x < expResult.size()-1;x++){
            SortableDeadlineObject result = DeadlineObjectsController.deadlineObjects.get(x);
            SortableDeadlineObject expResultitem = expResult.get(x);
            if(!expResultitem.getName().equals(result.getName())){
                fail();  
            }
            if(!expResultitem.getDeadline().equals(result.getDeadline())){
                fail();  
            }
            if(!expResultitem.getType().equals(result.getType())){
                fail();  
            }
            if(!expResultitem.getCompletionStatus().equals(result.getCompletionStatus())){
                fail();  
            }
            if(!expResultitem.isCompleted() ==result.isCompleted()){
                fail();  
            }
        }
    }
}
