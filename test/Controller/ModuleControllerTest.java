/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Module;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author James Baxter
 */
public class ModuleControllerTest {

    public ModuleControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        UserController.addUser("Baxter", "password");
        try {
            UserController.logIn(0, "password");
        } catch (UserController.InvalidUserDetailException ex) {
            System.out.println(ex.getMessage());
        }
        try {
            SemesterProfileController.newProfile(2017, 'A');
        } catch (SemesterProfileController.InvalidSemesterProfileException ex) {
            System.out.println(ex.getMessage());
        }
       
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ModuleController.addModule(new Module("0001", "TestName1", 10));
        ModuleController.addModule(new Module("0002", "TestName2", 20));
        ModuleController.addModule(new Module("0003", "TestName3", 30));
    }

    @After
    public void tearDown() {
        ModuleController.loadModules(new ArrayList<>());
    }

    /**
     * Test of loadModules method, of class ModuleController.
     */
    @Test
    public void testLoadModules_ArrayList() {
        System.out.println("loadModules");
        ArrayList<Module> modules = new ArrayList<>();
        modules.add(new Module("0004", "TestName4", 10));
        modules.add(new Module("0004", "TestName5", 50));
        ModuleController.loadModules(modules);
        ModuleController.setActiveModule(0);
        if (!ModuleController.activeModule.getModuleName().equals("TestName4")) {
            fail();
        }
         ModuleController.setActiveModule(1);
        if(ModuleController.activeModule.getModuleName().equals("TestName6")){
            fail();
        }
        assertTrue(true);
    }

    /**
     * Test of loadModules method, of class ModuleController.
     */
    @Test
    public void testLoadModules_0args() {
        System.out.println("loadModules");
        ModuleController.loadModules();
        if (!ModuleController.activeModule.getModuleName().equals("TestName1")) {
            fail();
        }
        assertTrue(true);
    }

    /**
     * Test of listModuleNames method, of class ModuleController.
     */
    @Test
    public void testListModuleNames() {
        System.out.println("listModuleNames");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("TestName1");
        expResult.add("TestName2");
        expResult.add("TestName3");
        ArrayList<String> result = ModuleController.listModuleNames();
        assertEquals(expResult, result);
    }

    /**
     * Test of setActiveProfile method, of class ModuleController.
     */
    @Test
    public void testSetActiveProfile() {
        System.out.println("setActiveProfile");
        int i = 0;
        ModuleController.setActiveModule(i);
        ModuleController.setActiveModule(0);
        if (!ModuleController.activeModule.getModuleName().equals("TestName1")) {
            fail();
        }
         ModuleController.setActiveModule(1);
        if(ModuleController.activeModule.getModuleName().equals("TestName3")){
            fail();
        }
        assertTrue(true);
    }

    /**
     * Test of addModule method, of class ModuleController.
     */
    @Test
    public void testAddModule_Module() {
        System.out.println("addModule");
        Module m = new Module("0004", "TestName4", 10);
        ModuleController.addModule(m);
        assertEquals(ModuleController.activeModule,m);
    }

    /**
     * Test of addModule method, of class ModuleController.
     */
    @Test
    public void testAddModule_3args() {
        System.out.println("addModule");
        String moduleCode = "0004";
        String title = "TestName4";
        int credit = 10;
        ModuleController.addModule(moduleCode, title, credit);
       assertEquals(ModuleController.activeModule.getModuleName(),"TestName4");
    }

}
