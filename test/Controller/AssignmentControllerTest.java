/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Controller.FileLoader.formatter;
import Model.Assignment;
import Model.Coursework;
import Model.Exam;
import Model.Module;
import Model.StudyTask;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import Model.MileStone;
import Model.StudyTask.ProgressTypes;
import Model.StudyTask.TaskTypes;

/**
 *
 * @author Shaun
 */
public class AssignmentControllerTest {
    
    public AssignmentControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        UserController.addUser("Baxter", "password");

        try {
            UserController.logIn(0, "password");
        } catch (UserController.InvalidUserDetailException ex) {
            System.out.println(ex.getMessage());
        }
        try {
            SemesterProfileController.newProfile(2017, 'A');
        } catch (SemesterProfileController.InvalidSemesterProfileException ex) {
            System.out.println(ex.getMessage());
        }
        Module mod = new Module("0001", "TestName1", 10);
        mod.addCoursework(new Coursework("Replaced Coursework", LocalDate.MAX, LocalDate.MIN));
        mod.addExam(new Exam("Replaced Exam", LocalDate.MIN, 60));
        //AssignmentController.getActiveAssignmentName();
        ModuleController.addModule(new Module("0001", "TestName1", 10));
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ArrayList<Coursework> coursework = new ArrayList<>();
        Coursework test1 = new Coursework("Test Coursework", LocalDate.parse("12/03/2017", formatter), LocalDate.parse("16/08/2017", formatter));
        Coursework test2 = new Coursework("Test2 Coursework", LocalDate.parse("03/05/2017", formatter), LocalDate.parse("16/06/2017", formatter));
        Coursework test3 = new Coursework("Test3 Coursework", LocalDate.parse("23/07/2017", formatter), LocalDate.parse("16/08/2017", formatter));
        Coursework test4 = new Coursework("Test4 Coursework", LocalDate.parse("30/09/2017", formatter), LocalDate.parse("16/10/2017", formatter));
        ArrayList<StudyTask> tasks = new ArrayList<>();
        test1.addMilestone(new MileStone("Milestone 1", LocalDate.parse("24/09/2017", formatter), tasks));
        test1.addMilestone(new MileStone("Milestone 2", LocalDate.parse("12/11/2017", formatter), tasks));
        test2.addMilestone(new MileStone("Milestone 3", LocalDate.parse("09/05/2017", formatter), tasks));
        test3.addMilestone(new MileStone("Milestone 4", LocalDate.parse("19/06/2017", formatter), tasks));
        test4.addMilestone(new MileStone("Milestone 5", LocalDate.parse("01/07/2017", formatter), tasks));
        coursework.add(test1);
        coursework.add(test2);
        coursework.add(test3);
        coursework.add(test4);
        
        ArrayList<Exam> exams = new ArrayList<>();
        Exam exam1 = new Exam("Test Exam", LocalDate.parse("06/05/2017", formatter), 120);
        Exam exam2 = new Exam("Test2 Exam", LocalDate.parse("12/05/2017", formatter), 120);
        Exam exam3 = new Exam("Test3 Exam", LocalDate.parse("26/04/2017", formatter), 120);
        Exam exam4 = new Exam("Test4 Exam", LocalDate.parse("17/05/2017", formatter), 120);
        exam1.addMilestone(new MileStone("Milestone 6", LocalDate.parse("01/12/2017", formatter), tasks));
        exam1.addMilestone(new MileStone("Milestone 7", LocalDate.parse("20/01/2017", formatter), tasks));
        exam2.addMilestone(new MileStone("Milestone 8", LocalDate.parse("09/06/2017", formatter), tasks));
        exam3.addMilestone(new MileStone("Milestone 9", LocalDate.parse("12/07/2017", formatter), tasks));
        exam4.addMilestone(new MileStone("Milestone 10", LocalDate.parse("21/03/2017", formatter), tasks));
        exam3.addMilestone(new MileStone("Milestone 11", LocalDate.parse("29/05/2017", formatter), tasks));
        
        StudyTask temp = new StudyTask("Make test Classes",LocalDate.parse("13/03/2017", formatter), LocalDate.parse("13/05/2017", formatter), 10,new ArrayList<>(), TaskTypes.PROGRAMMING, ProgressTypes.WRITING);
        StudyTask temp2 = new StudyTask("Make More test Classes",LocalDate.parse("14/03/2017", formatter), LocalDate.parse("12/05/2017", formatter), 3,new ArrayList<>(), TaskTypes.PROGRAMMING, ProgressTypes.WRITING);
        
        exam1.addTask(temp);
        exam1.addTask(temp2);
        
        exams.add(exam1);
        exams.add(exam2);
        exams.add(exam3);
        exams.add(exam4);
        
        AssignmentController.loadAssignments(coursework, exams);
        AssignmentController.loadSemesterAssignments(coursework, exams);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of loadAssignments method, of class AssignmentController.
     * @throws Controller.AssignmentController.InvalidAssignmentException
     */
    @Test
    public void testLoadAssignments() throws AssignmentController.InvalidAssignmentException {
        System.out.println("loadAssignments");
        AssignmentController.addCoursework("Replaced Coursework", LocalDate.MAX, LocalDate.MIN);
        AssignmentController.addExam("Replaced Exam", LocalDate.MIN, 60);
        
        ArrayList<Coursework> coursework = new ArrayList<>();
        coursework.add( new Coursework("Test Coursework", LocalDate.parse("01/09/2017", formatter), LocalDate.parse("01/09/2017", formatter)));
        
        ArrayList<Exam> exams = new ArrayList<>();
        exams.add(new Exam("Test Exam", LocalDate.parse("04/09/2017", formatter), 120));
        
        AssignmentController.loadAssignments(coursework, exams);
        AssignmentController.setActiveAssignment('c', 0);
        if(!(AssignmentController.getActiveAssignment().getAssignmentName().equals("Test Coursework"))){
            fail();
        }
        
        AssignmentController.setActiveAssignment('e', 0);
        if(!(AssignmentController.getActiveAssignment().getAssignmentName().equals("Test Exam"))){
            fail();
        }
    }

    /**
     * Test of loadSemesterAssignments method, of class AssignmentController.
     */
    @Test
    public void testLoadSemesterAssignments() {
        System.out.println("loadSemesterAssignments");
        
        ArrayList<Coursework> coursework = new ArrayList<>();
        coursework.add( new Coursework("Test Coursework", LocalDate.parse("01/09/2017", formatter), LocalDate.parse("01/09/2017", formatter)));
        
        ArrayList<Exam> exams = new ArrayList<>();
        exams.add(new Exam("Test Exam", LocalDate.parse("04/09/2017", formatter), 120));
        
        AssignmentController.loadSemesterAssignments(coursework, exams);
        
        ArrayList<String> test =AssignmentController.listAssignmentNamesByDateSemester();
 
        if(!(test.get(0).equals("Test Coursework"))){
            fail();
        }
        
        if(!(test.get(1).equals("Test Exam"))){
            fail();
        }
    }

    /**
     * Test of loadCoursework method, of class AssignmentController.
     */
    @Test
    public void testLoadCoursework() {
        System.out.println("loadCoursework");
        ArrayList<Coursework> coursework = new  ArrayList<>();
        coursework.add( new Coursework("Test Coursework", LocalDate.parse("10/09/2017", formatter), LocalDate.parse("03/09/2017", formatter)));
        AssignmentController.loadCoursework(coursework);
                String test = AssignmentController.getActiveAssignmentName();
        // TODO review the generated test code and remove the default call to fail.
        if (!(AssignmentController.getActiveAssignmentName().equals("Test Coursework"))){
            fail();
        }
        
    }

    /**
     * Test of loadExams method, of class AssignmentController.
     */
    @Test
    public void testLoadExams() {
        System.out.println("loadExams");
        ArrayList<Exam> exams = new ArrayList<>();
        exams.add(new Exam("LoadTest1", LocalDate.parse("26/06/2017", formatter), 60));
        exams.add(new Exam("LoadTest2", LocalDate.parse("26/05/2017", formatter), 120));
        AssignmentController.loadExams(exams);
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("LoadTest1");
        expResult.add("LoadTest2");
        ArrayList<String> result = AssignmentController.listExamNames();
        if(!result.equals(expResult)){
            fail();
        }
    }

    /**
     * Test of getActiveAssignmentName method, of class AssignmentController.
     */
    @Test
    public void testGetActiveAssignmentName() {
        System.out.println("getActiveAssignmentName");
        String expResult = "Test Exam";
        String result = AssignmentController.getActiveAssignmentName();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getActiveAssignmentType method, of class AssignmentController.
     */
    @Test
    public void testGetActiveAssignmentType() {
        System.out.println("getActiveAssignmentType");
        String expResult = "Exam";
        String result = AssignmentController.getActiveAssignmentType();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getActiveAssignment method, of class AssignmentController.
     * @throws Controller.AssignmentController.InvalidAssignmentException
     */
    @Test
    public void testGetActiveAssignment() throws AssignmentController.InvalidAssignmentException {
        System.out.println("getActiveAssignment");
        AssignmentController.setActiveAssignment('e', 0);
        String expResult = "Test Exam";
        String result = AssignmentController.getActiveAssignment().getAssignmentName();
        if (!expResult.equals(result)){
            fail();
        }
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getActiveAssignmentDateSet method, of class AssignmentController.
     */
    @Test
    public void testGetActiveAssignmentDateSet() throws AssignmentController.InvalidAssignmentException {
        System.out.println("getActiveAssignmentDateSet");
        String expResult = "12/03/2017";
        AssignmentController.setActiveAssignment('c', 0);
        String result = AssignmentController.getActiveAssignmentDateSet();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getActiveAssignmentDuration method, of class AssignmentController.
     */
    @Test
    public void testGetActiveAssignmentDuration() {
        System.out.println("getActiveAssignmentDuration");
        String expResult = Integer.toString(120);
        String result = AssignmentController.getActiveAssignmentDuration();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
    }

    /**
     * Test of getActiveAssignmentDeadLine method, of class AssignmentController.
     */
    @Test
    public void testGetActiveAssignmentDeadLine() {
        System.out.println("getActiveAssignmentDeadLine");
        String expResult = "06/05/2017";
        String result = AssignmentController.getActiveAssignmentDeadLine();
        if(!result.equals(expResult)){
             fail();
        }
    }

    /**
     * Test of listExamNames method, of class AssignmentController.
     */
    @Test
    public void testListExamNames() {
        System.out.println("listExamNames");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Test Exam");
        expResult.add("Test2 Exam");
        expResult.add("Test3 Exam");
        expResult.add("Test4 Exam");
        ArrayList<String> result = AssignmentController.listExamNames();
        if(!result.equals(expResult)){
             fail();
        }
    }

    /**
     * Test of listExamNamesByDate method, of class AssignmentController.
     */
    @Test
    public void testListExamNamesByDate() {
        System.out.println("listExamNamesByDate");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Test3 Exam");
        expResult.add("Test Exam");
        expResult.add("Test2 Exam");
        expResult.add("Test4 Exam");
        ArrayList<String> result = AssignmentController.listExamNamesByDate();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
    }

    /**
     * Test of sort method, of class AssignmentController.
     */
    @Test
    public void testSort() {
        System.out.println("sort");
        AssignmentController.sort();
        ArrayList<String> expResultExam = new ArrayList<>();
        expResultExam.add("Test3 Exam");
        expResultExam.add("Test Exam");
        expResultExam.add("Test2 Exam");
        expResultExam.add("Test4 Exam");
        
        ArrayList<String> expResultoursework = new ArrayList<>();
        expResultoursework.add("Test2 Coursework");
        expResultoursework.add("Test Coursework");
        expResultoursework.add("Test3 Coursework");
        expResultoursework.add("Test4 Coursework");
        
        ArrayList<String> resultExam = AssignmentController.listExamNames();
        ArrayList<String> resultCoursework = AssignmentController.listCourseworkNames();
        if(!resultExam.equals(expResultExam)){
            fail();
        }
        
        if(!resultCoursework.equals(expResultoursework)){
            fail();
        }
    }

    /**
     * Test of listCourseworkNames method, of class AssignmentController.
     */
    @Test
    public void testListCourseworkNames() {
        System.out.println("listCourseworkNames");
        ArrayList<String> expResult = new  ArrayList<>();
        expResult.add("Test Coursework");
        expResult.add("Test2 Coursework");
        expResult.add("Test3 Coursework");
        expResult.add("Test4 Coursework");
        ArrayList<String> result = AssignmentController.listCourseworkNames();
        if (!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listCourseworkNamesByDate method, of class AssignmentController.
     */
    @Test
    public void testListCourseworkNamesByDate() {
        System.out.println("listCourseworkNamesByDate");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Test2 Coursework");
        expResult.add("Test Coursework");
        expResult.add("Test3 Coursework");
        expResult.add("Test4 Coursework");
        ArrayList<String> result = AssignmentController.listCourseworkNamesByDate();
        if (!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listAssignmentNamesByDateSemester method, of class AssignmentController.
     */
    @Test
    public void testListAssignmentNamesByDateSemester() {
        System.out.println("listAssignmentNamesByDateSemester");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Test3 Exam");
        expResult.add("Test Exam");
        expResult.add("Test2 Exam");
        expResult.add("Test4 Exam");
        expResult.add("Test2 Coursework");
        expResult.add("Test Coursework");
        expResult.add("Test3 Coursework");
        expResult.add("Test4 Coursework");
        ArrayList<String> result = AssignmentController.listAssignmentNamesByDateSemester();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listMileStonesNamesByDate method, of class AssignmentController.
     */
    @Test
    public void testListMileStonesNamesByDate() {
        //AssignmentController.loa
        
        System.out.println("listMileStonesNamesByDate");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Milestone 7");
        expResult.add("Milestone 10");
        expResult.add("Milestone 3");
        expResult.add("Milestone 11");
        expResult.add("Milestone 8");
        expResult.add("Milestone 4");
        expResult.add("Milestone 5");
        expResult.add("Milestone 9");
        expResult.add("Milestone 1");
        expResult.add("Milestone 2");
        expResult.add("Milestone 6");
        ArrayList<String> result = AssignmentController.listMileStonesNamesByDate();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listAssignmentNamesWithDateSemester method, of class AssignmentController.
     */
    @Test
    public void testListAssignmentNamesWithDateSemester() {
        System.out.println("listAssignmentNamesWithDateSemester");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Test3 Exam 2017-04-26");
        expResult.add("Test Exam 2017-05-06");
        expResult.add("Test2 Exam 2017-05-12");
        expResult.add("Test4 Exam 2017-05-17");
        expResult.add("Test2 Coursework 2017-06-16");
        expResult.add("Test Coursework 2017-08-16");
        expResult.add("Test3 Coursework 2017-08-16");
        expResult.add("Test4 Coursework 2017-10-16");
        ArrayList<String> result = AssignmentController.listAssignmentNamesWithDateSemester();
        
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listAssignmentNamesByDate method, of class AssignmentController.
     */
    @Test
    public void testListAssignmentNamesByDate() {
        System.out.println("listAssignmentNamesByDate");
        ArrayList<String> expResult = new  ArrayList<>();
        expResult.add("Test3 Exam");
        expResult.add("Test Exam");
        expResult.add("Test2 Exam");
        expResult.add("Test4 Exam");
        expResult.add("Test2 Coursework");
        expResult.add("Test Coursework");
        expResult.add("Test3 Coursework");
        expResult.add("Test4 Coursework");
        ArrayList<String> result = AssignmentController.listAssignmentNamesByDate();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listAssignmentNamesWithDate method, of class AssignmentController.
     */
    @Test
    public void testListAssignmentNamesWithDate() {
        System.out.println("listAssignmentNamesWithDate");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Test3 Exam 2017-04-26");
        expResult.add("Test Exam 2017-05-06");
        expResult.add("Test2 Exam 2017-05-12");
        expResult.add("Test4 Exam 2017-05-17");
        expResult.add("Test2 Coursework 2017-06-16");
        expResult.add("Test Coursework 2017-08-16");
        expResult.add("Test3 Coursework 2017-08-16");
        expResult.add("Test4 Coursework 2017-10-16");
        ArrayList<String> result = AssignmentController.listAssignmentNamesWithDate();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of addCoursework method, of class AssignmentController.
     */
    @Test
    public void testAddCoursework_Coursework() {
        System.out.println("addCoursework");
        Coursework c = new Coursework("addCourseworkTest", 
                LocalDate.parse("24/09/2017", formatter), LocalDate.parse("24/09/2017", formatter));
        AssignmentController.addCoursework(c);
        String result = AssignmentController.getActiveAssignmentName();
        
        if(!c.getAssignmentName().equals(result)){
            fail();
        }
    }

    /**
     * Test of addCoursework method, of class AssignmentController.
     */
    @Test
    public void testAddCoursework_3args() {
        AssignmentController.getActiveAssignmentName();
        System.out.println("addCoursework_3args");
        String assignmentName = "addCourseworkTest_3args";
        LocalDate dateSet = LocalDate.parse("24/08/2017", formatter);
        LocalDate deadline = LocalDate.parse("24/09/2017", formatter);
        AssignmentController.addCoursework(assignmentName, dateSet, deadline);

        String result = AssignmentController.getActiveAssignmentName();
        
        if(!assignmentName.equals(result)){
            fail();
        }
    }

    /**
     * Test of addExam method, of class AssignmentController.
     */
    @Test
    public void testAddExam_Exam() {
        System.out.println("addExam");
        Exam e = new Exam("TestAddExam", LocalDate.parse("24/09/2017", formatter), 120);
        AssignmentController.addExam(e);
        String result = AssignmentController.getActiveAssignmentName();
        
        if(!e.getAssignmentName().equals(result)){
            fail();
        }
    }

    /**
     * Test of addExam method, of class AssignmentController.
     */
    @Test
    public void testAddExam_3args() {
        System.out.println("addExam_3args");
        String assignmentName = "TestAddExam_3args";
        LocalDate deadline = LocalDate.parse("24/10/2017", formatter);
        int duration = 90;
        AssignmentController.addExam(assignmentName, deadline, duration);
        
        String result = AssignmentController.getActiveAssignmentName();
        
        if(!assignmentName.equals(result)){
            fail();
        }
    }

    /**
     * Test of setActiveAssignment method, of class AssignmentController.
     * @throws java.lang.Exception
     */
    @Test
    public void testSetActiveAssignment_char_int() throws Exception {
        System.out.println("setActiveAssignment");
        char type = 'c';
        int i = 0;
        AssignmentController.setActiveAssignment(type, i);
        
        if (!(AssignmentController.getActiveAssignmentName().equals("Test Coursework"))){
            fail();
        }
        
        type = 'e';
        AssignmentController.setActiveAssignment(type, i);
        
        if (!(AssignmentController.getActiveAssignmentName().equals("Test Exam"))){
            fail();
        }
    }

    /**
     * Test of setActiveAssignment method, of class AssignmentController.
     */
    @Test
    public void testSetActiveAssignment_char_String() throws Exception {
        System.out.println("setActiveAssignment");
        char type = 'c';
        String title = "Test4 Coursework";
        AssignmentController.setActiveAssignment(type, title);
        String result = AssignmentController.getActiveAssignmentName();
        if(!title.equals(result)){
            fail();
        }
    }

    /**
     * Test of setActiveAssignment method, of class AssignmentController.
     */
    @Test
    public void testSetActiveAssignment_String() throws Exception {
        System.out.println("setActiveAssignment");
        String title = "Test3 Exam";
        AssignmentController.setActiveAssignment(title);
        String result = AssignmentController.getActiveAssignmentName();
        if(!result.equals(title)){
            fail();
        }
    }

    /**
     * Test of getStudyTasks method, of class AssignmentController.
     */
    @Test
    public void testGetStudyTasks() {
        System.out.println("getStudyTasks");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Make test Classes");
        expResult.add("Make More test Classes");
        ArrayList<String> result = AssignmentController.getStudyTasks();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getMileStones method, of class AssignmentController.
     */
    @Test
    public void testGetMileStones() {
        System.out.println("getMileStones");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Milestone 6");
        expResult.add("Milestone 7");
        ArrayList<String> result = AssignmentController.getMileStones();
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of listMileStonesNamesWithDate method, of class AssignmentController.
     */
    @Test
    public void testListMileStonesNamesWithDate() {
        System.out.println("listMileStonesNamesWithDate");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("Milestone 7 2017-01-20");
        expResult.add("Milestone 10 2017-03-21");
        expResult.add("Milestone 3 2017-05-09");
        expResult.add("Milestone 11 2017-05-29");
        expResult.add("Milestone 8 2017-06-09");
        expResult.add("Milestone 4 2017-06-19");
        expResult.add("Milestone 5 2017-07-01");
        expResult.add("Milestone 9 2017-07-12");
        expResult.add("Milestone 1 2017-09-24");
        expResult.add("Milestone 2 2017-11-12");
        expResult.add("Milestone 6 2017-12-01");
        ArrayList<String> result = AssignmentController.listMileStonesNamesWithDate();
        if(!result.equals(expResult)){
            fail();
        }
    }
}
