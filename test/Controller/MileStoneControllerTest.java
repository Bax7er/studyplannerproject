/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import static Controller.FileLoader.formatter;
import Model.Coursework;
import Model.MileStone;
import Model.StudyTask;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Shaun
 */
public class MileStoneControllerTest {
    
    public MileStoneControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws StudyTaskController.InvalidStudyTaskException {
        Coursework coursework1 = new Coursework("Software Engineering", LocalDate.parse("13/03/2017", formatter), LocalDate.parse("17/05/2017", formatter));
        //coursework1.addTask(task);
        AssignmentController.addCoursework(coursework1);
        ArrayList<StudyTask> tasks1 = new ArrayList<>();
        //"Make test Classes",LocalDate.parse("13/03/2017", formatter), LocalDate.parse("13/05/2017", formatter), 10,new ArrayList<>(), 1, 3
        tasks1.add(new StudyTask("Make test Classes1", LocalDate.parse("13/03/2017", formatter), LocalDate.parse("13/05/2017", formatter), 10, new ArrayList<>(), StudyTask.TaskTypes.PROGRAMMING, StudyTask.ProgressTypes.WRITING));
        tasks1.add(new StudyTask("Make test Classes2", LocalDate.parse("14/03/2017", formatter), LocalDate.parse("14/05/2017", formatter), 10, new ArrayList<>(), StudyTask.TaskTypes.PROGRAMMING, StudyTask.ProgressTypes.WRITING));
        coursework1.addTask(tasks1.get(0));
        coursework1.addTask(tasks1.get(1));
        
        ArrayList<StudyTask> tasks2 = new ArrayList<>();
        tasks2.add(new StudyTask("Make test Classes3", LocalDate.parse("15/03/2017", formatter), LocalDate.parse("15/05/2017", formatter), 10, new ArrayList<>(), StudyTask.TaskTypes.PROGRAMMING, StudyTask.ProgressTypes.WRITING));
        tasks2.add(new StudyTask("Make test Classes4", LocalDate.parse("16/03/2017", formatter), LocalDate.parse("16/05/2017", formatter), 10, new ArrayList<>(), StudyTask.TaskTypes.PROGRAMMING, StudyTask.ProgressTypes.WRITING));
        tasks2.add(new StudyTask("Make test Classes5", LocalDate.parse("17/03/2017", formatter), LocalDate.parse("17/05/2017", formatter), 10, new ArrayList<>(), StudyTask.TaskTypes.PROGRAMMING, StudyTask.ProgressTypes.WRITING));
        coursework1.addTask(tasks2.get(0));
        coursework1.addTask(tasks2.get(1));
        coursework1.addTask(tasks2.get(2));
        
        ArrayList<MileStone> milestoneList = new ArrayList<>();

        milestoneList.add(new MileStone("MileStone1", LocalDate.parse("20/05/2017", formatter), tasks1));
        milestoneList.add(new MileStone("MileStone2", LocalDate.parse("30/05/2017", formatter), tasks2));
        
        coursework1.addMilestone(milestoneList.get(0));
        coursework1.addMilestone(milestoneList.get(1));
        
        MileStoneController.loadMilestone(coursework1.getMilestones());
        MileStoneController.setActiveMileStone(0);
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of loadMilestone method, of class MileStoneController.
     */
    @Test
    public void testLoadMilestone() {
        System.out.println("loadMilestone");
        ArrayList<StudyTask> tasks1 = new ArrayList<>();
        tasks1.add(new StudyTask());
        tasks1.add(new StudyTask());
        tasks1.add(new StudyTask());
        
        ArrayList<StudyTask> tasks2 = new ArrayList<>();
        tasks2.add(new StudyTask());
        tasks2.add(new StudyTask());
        tasks2.add(new StudyTask());
        
        ArrayList<MileStone> milestoneList = new ArrayList<>();
        milestoneList.add(new MileStone("MileStone1", LocalDate.parse("03/05/2017", formatter), tasks1));
        milestoneList.add(new MileStone("MileStone2", LocalDate.parse("03/05/2017", formatter), tasks2));
        
        MileStoneController.loadMilestone(milestoneList);
        if(!milestoneList.equals(MileStoneController.milestones)){
            fail();
        }
    }

    /**
     * Test of setActiveMileStone method, of class MileStoneController.
     */
    @Test
    public void testSetActiveMileStone_int() {
        System.out.println("setActiveMileStone");
        int index = 1;
        MileStoneController.setActiveMileStone(index);
        String result = MileStoneController.getActiveMileStoneName();
        if(!result.equals("MileStone2")){
            fail();
        }
    }

    /**
     * Test of getActiveMileStoneName method, of class MileStoneController.
     */
    @Test
    public void testGetActiveMileStoneName() {
        System.out.println("getActiveMileStoneName");
        String expResult = "MileStone1";
        String result = MileStoneController.getActiveMileStoneName();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
    }

    /**
     * Test of getActiveMileStoneDeadLine method, of class MileStoneController.
     */
    @Test
    public void testGetActiveMileStoneDeadLine() {
        System.out.println("getActiveMileStoneDeadLine");
        String expResult = "2017-05-20";
        String result = MileStoneController.getActiveMileStoneDeadLine();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
    }

    /**
     * Test of getStudyTaskCompletionTable method, of class MileStoneController.
     */
    @Test
    public void testGetStudyTaskCompletionTable() {
        System.out.println("getStudyTaskCompletionTable");
        ArrayList<String[]> expResult = new ArrayList<>();
        expResult.add(new String[]{"Pending", "Make test Classes1"});
        expResult.add(new String[]{"Pending", "Make test Classes2"});
        ArrayList<String[]> result = MileStoneController.getStudyTaskCompletionTable();
        for(int x =0;x < expResult.size()-1;x++){
            for(int y =0;y < expResult.size()-1;y++){ 
                if(!expResult.get(x)[y].equals(result.get(x)[y])){
                    fail();
                }
            }
        }
        
        
        
    }

    /**
     * Test of getMilestoneNames method, of class MileStoneController.
     */
    @Test
    public void testGetMilestoneNames() {
        System.out.println("getMilestoneNames");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("MileStone1");
        expResult.add("MileStone2");
        ArrayList<String> result = MileStoneController.getMilestoneNames();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of getMilestoneNamesWithDates method, of class MileStoneController.
     */
    @Test
    public void testGetMilestoneNamesWithDates() {
        System.out.println("getMilestoneNamesWithDates");
        ArrayList<String> expResult = new ArrayList<>();
        expResult.add("MileStone12017-05-20");
        expResult.add("MileStone22017-05-30");
        ArrayList<String> result = MileStoneController.getMilestoneNamesWithDates();
        assertEquals(expResult, result);
        if(!expResult.equals(result)){
            fail();
        }
    }

    /**
     * Test of addMilestone method, of class MileStoneController.
     * @throws java.lang.Exception
     */
    @Test
    public void testAddMilestone() throws Exception {
        System.out.println("addMilestone");
        String name = "AddMilestoneTest";
        LocalDate deadline = LocalDate.parse("15/03/2017", formatter);
        ArrayList<StudyTask> dependencies = new ArrayList<>();
        dependencies.add(new StudyTask("Make test Classes1", LocalDate.parse("11/03/2017", formatter), LocalDate.parse("13/03/2017", formatter), 10, new ArrayList<>(), StudyTask.TaskTypes.PROGRAMMING, StudyTask.ProgressTypes.WRITING));
        MileStoneController.addMilestone(name, deadline, dependencies);
        MileStoneController.setActiveMileStone(MileStoneController.milestones.size()-1);
        
        String nameresult = MileStoneController.getActiveMileStoneName();
        String deadlineResult = MileStoneController.getActiveMileStoneDeadline();
        if(!nameresult.equals(name)){
            fail();
        }
        if(!deadlineResult.equals("15/03/2017")){
            fail();
        }
    }

    /**
     * Test of getTaskCount method, of class MileStoneController.
     */
    @Test
    public void testGetTaskCount() {
        System.out.println("getTaskCount");
        int expResult = 2;
        int result = MileStoneController.getTaskCount();
        assertEquals(expResult, result);
        if(expResult != result){
            fail();
        }
    }

    /**
     * Test of getCompletedDecimal method, of class MileStoneController.
     */
    @Test
    public void testGetCompletedDecimal() {
        System.out.println("getCompletedDecimal");
        float expResult = 0.0F;
        float result = MileStoneController.getCompletedDecimal();
        assertEquals(expResult, result, 0.0);
        if(expResult != result){
            fail();
        }
    }

    /**
     * Test of getCompletedCount method, of class MileStoneController.
     */
    @Test
    public void testGetCompletedCount() {
        System.out.println("getCompletedCount");
        int expResult = 0;
        int result = MileStoneController.getCompletedCount();
        assertEquals(expResult, result);
        if(expResult != result){
            fail();
        }
    }

    /**
     * Test of getActiveMileStoneDeadline method, of class MileStoneController.
     */
    @Test
    public void testGetActiveMileStoneDeadline() {
        System.out.println("getActiveMileStoneDeadLine");
        String expResult = "20/05/2017";
        String result = MileStoneController.getActiveMileStoneDeadline();
        assertEquals(expResult, result);
        if(!result.equals(expResult)){
            fail();
        }
    }
    /**
     * Test of setActiveMilestoneName method, of class MileStoneController.
     */
    @Test
    public void testSetActiveMilestoneName() throws Exception {
        System.out.println("setActiveMilestoneName");
        String text = "TestSetName";
        MileStoneController.setActiveMilestoneName(text);
        String result = MileStoneController.getActiveMileStoneName();
        if(!text.equals(result)){
            fail();
        }
    }

    /**
     * Test of setActiveMilestoneDeadline method, of class MileStoneController.
     */
    @Test
    public void testSetActiveMilestoneDeadline() throws Exception {
        System.out.println("setActiveMilestoneDeadline");
        String date = "15/05/2017";
        LocalDate deadline = LocalDate.parse(date, formatter);
        MileStoneController.setActiveMilestoneDeadline(deadline);
        String result = MileStoneController.getActiveMileStoneDeadline();
        if(!date.equals(result)){
            fail();
        }
    } 
}
