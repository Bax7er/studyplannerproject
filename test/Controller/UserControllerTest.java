/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.User;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Baxter
 */
public class UserControllerTest {
    
    public UserControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        UserController.addUser(new User("Dave",1));
        UserController.addUser(new User("Bob",2));
        UserController.addUser(new User("Jim",3));
        String username = "James";
        String password = "password";
        UserController.addUser(username, password);
    }
    
    @After
    public void tearDown() {
        UserController.clear();
    }

    /**
     * Test of getUserNames method, of class UserController.
     */
    @Test
    public void testGetUserNames() {
        System.out.println("getUserNames");
        ArrayList<String> expResult = new ArrayList<String>();
        expResult.add("Dave");
        expResult.add("Bob");
        expResult.add("Jim");
         expResult.add("James");
        ArrayList<String> result = UserController.getUserNames();
        assertEquals(expResult, result);
    }

    /**
     * Test of addUser method, of class UserController.
     */
    @Test
    public void testAddUser_User() {
        System.out.println("addUser");
        User u = new User("Rob",4);
        UserController.addUser(u);
    }

    /**
     * Test of addUser method, of class UserController.
     */
    @Test
    public void testAddUser_String_charArr() {
        System.out.println("addUser");
        String username = "James";
        String password = "password";
        char[] cpassword = password.toCharArray();
        UserController.addUser(username, cpassword);
    }

    /**
     * Test of addUser method, of class UserController.
     */
    @Test
    public void testAddUser_String_String() {
        System.out.println("addUser");
        String username = "Shaun";
        String password = "password";
        UserController.addUser(username, password);

    }

    /**
     * Test of positive checkPassword method, of class UserController.
     */
    @Test
    public void testCheckPassword() {
        System.out.println("checkPassword");
        int userNo = 3;
        String password = "password";
        boolean expResult = true;
        boolean result = UserController.checkPassword(userNo, password);
        assertEquals(expResult, result);
        
    }
    /**
     * Test of negative checkPassword method, of class UserController.
     */
    @Test
    public void testCheckPasswordFail() {
        System.out.println("checkPassword");
        int userNo = 2;
       String password = "h";
        boolean expResult = false;
        boolean result = UserController.checkPassword(userNo, password);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of isValidUserName method, of class UserController.
     */
    @Test
    public void testIsValidUserName() {
        System.out.println("isValidUserName");
        String username = "Bob";
        boolean expResult = false;
        boolean result = UserController.isValidUserName(username);
        assertEquals(expResult, result);
        username = "Bill";
        expResult = true;
        result = UserController.isValidUserName(username);
        assertEquals(expResult, result);
        username = "";
        expResult = false;
        result = UserController.isValidUserName(username);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of isValidPassword method, of class UserController.
     */
    @Test
    public void testIsValidPassword() {
        System.out.println("isValidPassword");
        String password = "1234";
        boolean expResult = false;
        boolean result = UserController.isValidPassword(password);
        assertEquals(expResult, result);
        password = "12345";
        expResult = true;
        result = UserController.isValidPassword(password);
        assertEquals(expResult, result);
    }

    /**
     * Test of clear method, of class UserController.
     */
    @Test
    public void testClear() {
        System.out.println("clear");
        UserController.clear();
        
    }
    

}
